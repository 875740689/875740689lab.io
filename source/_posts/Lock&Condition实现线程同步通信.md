---
title: 线程同步相关内容
date: 2020-10-09 12:46:29
tags: 并发编程
---

### Lock&Condition实现线程同步通信

Lock比传统线程模型中的synchronized方式更加面向对象，且更具可操作性，能可中断的获取锁以及超时获取锁等多种synronized关键字所不具备的同步特性。<!--more-->

示例：

```java
static class Outputer{

	Lock lock = new ReentrantLock();

	public void output(String name){

		int len = name.length();
		//上锁
		lock.lock();
    	try{
			for(int i=0;i<len;i++){
				System.out.print(name.charAt(i));
			}
			System.out.println();
        }finally{
			//解锁
			lock.unlock();
		}
    }
}
```

读写锁（ReadWriteLock）：分为读锁和写锁，多个读锁不互斥，读锁与写锁互斥，写锁与写锁互斥

示例：

```java
class Queue3{
	private Object data = null;		//共享数据，只能有一个线程能写该数据，但可以有多个线程同时读该数据
	ReadWriteLock rwl = new ReentrantReadWriteLock();
	public void get(){
		rwl.readLock().lock();
		try{
			System.out.println(Thread.currentThread().getName()+" be 					ready to read data!");
			Thread.sleep((long)(Math.random()*1000));
			System.out.println(Thread.currentThread().getName()+" have 					read data :"+data);
		}catch(InterruptedException e){
			e.printStackTrace();
		}finally{
			rwl.readLock().unlock();
		}
	}
	
	public void put(Object data){
		rwl.writeLock().lock();
		try{
			System.out.println(Thread.currentThread().getName()+" be 					ready to write data!");
			Thread.sleep((long)(Math.random()*1000));
			this.data = data;
			System.out.println(Thread.currentThread().getName()+" have 					write data:"+data);
		}catch(InterruptedException e){
			e.printStackTrace();
		}finally{
			rwl.writeLock().unlock();
		}
	}
}
```

Condition的功能类似在传统线程技术中的Object.wait和Object.notify的功能。在等待Condition时，允许发生“虚假唤醒”。 

```java
static class Business {
		Lock lock = new ReentrantLock();
		Condition condition1 = lock.newCondition();
		Condition condition2 = lock.newCondition();
		private boolean bShouldSub = true;

		public void sub(int i) {
			lock.lock();
			try {
				while (!bShouldSub) {
					try {
						condition1.await();
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				for (int j = 1; j <= 10; j++) {
					System.out.println("sub thread sequece of " + j + 							",loop of " + i);
				}
				bShouldSub = false;
				condition2.signal();
				// this.notify();
			} finally {
				lock.unlock();
			}
		}

		public void main(int i) {
			lock.lock();
			try {
				while (bShouldSub) {
					try {
						condition2.await();
						//this.wait();
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				for (int j = 1; j <= 100; j++) {
					System.out.println("main thread sequece of " + j + 							",loop of " + i);
				}
				bShouldSub = true;
				condition1.signal();
				//this.notify();
			} finally {
				lock.unlock();
			}
		}
	}

```

一个锁内部可以有多个Condition，即有多路等待和通知，传统的线程机制中一个监视器对象上只能有一路等待和通知，要想实现多路等待和通知，必须嵌套使用多个同步监视器对象。

### Semaphore实现信号灯

Semaphore可以维护当前访问自身的线程个数，并提供了同步机制。

单个信号量的Semaphore对象可以实现互斥锁的功能，并且可以是由一个线程获得了“锁”，再由另一个线程释放“锁”，这可应用于死锁恢复的一些场合。

### 其他同步工具类

CyclicBarrier	循环障碍

CountDownLatch	倒计时器 可以实现一个人（也可以是多个人）等待其他所有人都来通知他，可以实现一个人通知多个人的效果。

Exchanger	用于实现两个人之间的数据交换，每个人再完成一定的事务后想与对方交换数据，第一个先拿出数据的人将一直等待第二个人拿着数据到来时，才能彼此交换数据。

### 可阻塞的队列

队列包含固定长度的队列和不固定长度的队列

ArrayBlockingQueue（阻塞队列）	其中只有put方法和take方法才具有阻塞功能

阻塞队列和Semaphore有些相似，但也有不同

### 同步集合

传统方式下用Collections工具类提供的synchronizedCollection方法来获得同步集合。

Java5开始提供了如下一些同步集合类：

ConcurrentHashMap，CopyOnWriteArrayList，CopyOnWriteArraySet



wait，notify和notifyAll只能在同步控制方法或者同步控制块里面使用，而sleep可以在任何地方使用。sleep是线程类（Thread）的方法，不会释放锁；wait是Object类的方法，会释放锁。