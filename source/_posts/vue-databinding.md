﻿---
title: vue中的数据双向绑定
//toc: true
//thumbnail: img/bind.jpg
date: 2020-04-06 13:47:26
tags:
 - Vue
 - mvvm
 - 数据绑定
categories:
 - Vue
---

## 前言

什么是双向数据绑定？
当数据发生变化时，视图也会随之发生变化，当视图发生变化时，数据也会随着视图同步变化。数据双向绑定时对于UI控件来说的，非UI控件不会涉及到数据双向绑定。
<!--more-->
全局性数据流使用单向，方便跟踪；局部性数据流使用双向，简单易操作。

## 实现数据双向绑定
Vue提供了v-model指令实现数据双向绑定功能
```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <script src="./lib/vue-2.4.0.js"></script>
</head>
<body>
<div id="app">
    <h4>{{msg}}</h4>

    <!--v-bind只能实现数据的单项绑定，从M自动绑定到V，无法实现数据的双向绑定-->
    <!--    <input type="text" v-bind:value="msg" style="width:100%">-->

    <input type="text" style="width:100%" v-model="msg">
</div>
<script>
    //创建Vue实例，得到ViewModel
    var vm=new Vue({
        el: '#app',
        data: {
            msg: 'helloworld'
        },
        methods: {

        }
    });
</script>
</body>
</html>
```
需要注意的是，v-model只能应用在表单元素中

