﻿---
title: API文档生成工具Swagger2的使用
//toc: true
//thumbnail: 
date: 2020-04-03 11:16:23
tags: 
 - API文档自动生成工具
 - 前后端分离
categories:
 - 前后端分离
---

在前后端分离的开发模式下，前后端系统通过接口进行交互，API接口文档变成了前后端开发人员联系的纽带，变得越来越重要，因此许多的API接口文档自动生成工具开始进入我们的视野。<!--more-->Swagger2是一个规范和完整的框架，用于生成、描述、调用和可视化RESTful风格的Web服务。它能够实时同步api与文档，但它的代码侵入性比较强，会影响正常代码阅读。


## SpringBoot集成Swagger2

### 1、在pom.xml中添加依赖

```pom
<dependency>
	<groupId>io.springfox</groupId>
	<artifactId>springfox-swagger2</artifactId>
	<version>2.9.2</version>
</dependency>
<dependency>
	<groupId>io.springfox</groupId>
	<artifactId>springfox-swagger-ui</artifactId>
	<version>2.9.2</version>
</dependency>
```

### 2、Swagger2配置类

```java
@Configuration
@EnableSwagger2
public class Swagger2Config{
    @Bean
    public Docket createRestApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.jenson.ecommerce"))
                .paths(PathSelectors.any())
                .build();
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("标题")
                .description("相关描述")
                .contact(new Contact("name","url","email")) 
		.version("1.0")
                .build();
    }
}
```

### 3、在开发中使用相关注解

#### (1)@Api

使用在Controller层Api类上，主要属性有tags(标签)、hidden(是否隐藏)、value、authorizations等。

#### (2)@ApiOperation

使用在Api类的接口方法上，主要属性有value(接口名称)、notes(注释)、hidden(是否隐藏)、httpMethod、ignoreJsonView、response、responseHeaders等等，某些属性注解可自动识别，无需配置。

#### (3)@ApiImplicitParams、@ApiImplicitParam

使用在Api类的接口方法上，对接口参数进行说明，@ApiImplicitParams只有一个属性value，@ApiImplicitParam主要属性有name(参数名称)、value(参数说明)、required(是否必需)、dataType(数据类型)、paramType(参数类型)、dataTypeClass、defaultValue、readOnly等。

#### (4)@ApiModel

用在实体类上，主要属性有description(描述)、parent(父类)、subTypes、value、discriminator等。

#### (5)@ApiModelProperty

用在实体类属性上，主要属性有access、accessMode、allowableValues、allowEmptyValue(是否允许为空)、dataType(数据类型)、example(示例)、hidden(是否隐藏)、name(名称)、notes、required(是否必需)、value(说明)等。

注意：要保证实体类属性都有相应的get、set方法，否则swagger-ui页面无该属性说明。

### 4、打开swagger-ui界面

运行项目后，登录localhost:项目端口号/swagger-ui.html。
