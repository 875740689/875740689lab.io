﻿---
title: Elasticsearch的使用
//toc: true
//thumbnail: img/elasticsearch.jpg
date: 2020-04-04 20:55:58
tags: 
 - Elasticsearch
categories:
 - 后端开发
---

## 用数据库可以实现搜索功能，为什么还需要Elasticsearch呢？

在使用数据库搜索时，我们更多的是基于精确匹配的搜索，并不支持相关性匹配，与精确匹配相比，相关性匹配更贴近人的思维方式，而Elasticsearch支持相关性匹配。<!--more-->
搜索引擎不只是搜索，还有分析，分析数据的能力，是建立在快速的查询上的，而Elasticsearch之所以能够快速查询，是因为Elasticsearch基于倒排索引，对于文档搜索来说，倒排索引在性能和空间上都有更加明显的优势。Elastic的优势还有：支持中文分词插件。

注：Elasticsearch存储数据的方式是文档存储，把对象原原本本地放进去，取出时直接取出。

## 在Linux上使用docker安装Elasticsearch

### 1、安装Docker

Docker要求CentOS系统的内核版本高于3.10.
查看你当前的内核版本
```linux
uname -r
```
若内核版本较低，可升级内核
```linux
sudo yum update
```
安装Docker
```linux
yum install docker
```
设置Docker为开机启动
```linux
systemctl enable docker
```
启动Docker
```linux
systemctl start docker
```

### 2、在Docker上部署Elasticsearch

#### 2.1、安装Elasticsearch

下载Elasticsearch镜像
```linux
docker pull elasticsearch
```
查看镜像
```linux
docker images
```
运行Elasticsearch镜像
```linux
docker run -e ES_JAVA_OPTS="-Xms256m -Xmx256m" -d -p 9200:9200 -p 9300:9300 --name containerName imageId
```
查看正在运行的容器
```linux
docker ps
```
在浏览器中打开http://服务器IP地址:9200,如果看到以下信息则说明安装成功
```json
{
  "name" : "530dd7820315",
  "cluster_name" : "docker-cluster",
  "cluster_uuid" : "7O0fjpBJTkmn_axwmZX0RQ",
  "version" : {
    "number" : "7.2.0",
    "build_flavor" : "default",
    "build_type" : "docker",
    "build_hash" : "508c38a",
    "build_date" : "2019-06-20T15:54:18.811730Z",
    "build_snapshot" : false,
    "lucene_version" : "8.0.0",
    "minimum_wire_compatibility_version" : "6.8.0",
    "minimum_index_compatibility_version" : "6.0.0-beta1"
  },
  "tagline" : "You Know, for Search"
}
```

#### 2.2、修改配置，解决跨域访问问题
首先进入到容器中，然后进入到指定目录修改elasticsearch.yml文件。
```linux
docker exec -it elasticsearch /bin/bash
cd /usr/share/elasticsearch/config/
vi elasticsearch.yml
```
在elasticsearch.yml的文件末尾加上
```
http.cors.enabled: true
http.cors.allow-origin: "*"
```
修改配置后重启容器即可
```linux
docker restart elasticsearch
```

#### 2.3、安装ik分词器
es自带的分词器对中文分词不是很友好，所以我们下载开源的IK分词器来解决这个问题。首先进入到plugins目录中下载分词器，下载完成后然后解压，在重启es即可
```
cd /usr/share/elasticsearch/plugins/
elasticsearch-plugin install https://github.com/medcl/elasticsearch-analysis-ik/releases/download/v7.2.0/elasticsearch-analysis-ik-7.2.0.zip
exit
docker restart elasticsearch
```
注意：elasticsearch的版本和ik分词器的版本需要保持一致，不然在重启时会失败。可以在这查看所有版本，选择适合自己版本的右键复制链接地址即可。[点击这里](https://github.com/medcl/elasticsearch-analysis-ik/releases)

### 3、用Docker安装kibana
安装kibana
```linux
docker pull kibana
```
启动kibana
```linux
docker run --name kibana --link=elasticsearch:test  -p 5601:5601 -d kibana:7.2.0
docker start kibana
```
启动之后可以打开浏览器输入http://服务器IP地址:5601可以打开kibana的界面。
