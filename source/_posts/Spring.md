---
title: Spring
date: 2020-10-18 14:18:31
tags: Java
---

## 一、Spring概述

1、Spring是轻量级的开源的JavaEE框架

2、Spring可以解决企业应用开发的复杂性

3、Spring有两个核心部分：IOC和AOP

（1）IOC：控制反转，把创建对象过程 交给Spring管理

（2）AOP：面向切面，不修改源代码的情况下进行功能增强。<!--more-->

4、Spring特点：

（1）方便解耦，简化开发

（2）AOP编程支持

（3）方便程序测试

（4）方便和其他框架进行整合

（5）方便进行事务操作

（6）降低JavaEE开发难度

5、当前笔记针对Spring版本5.x

### 入门案例

1、下载Spring5

（1）使用Spring5.2.6

（2）下载地址

https://repo.spring.io/release/org/springframework/spring/

2、打开idea工具，创建普通java工程

3、导入Spring5相关jar包

4、创建普通类，在这个类中创建普通方法

5、创建Spring配置文件，在配置文件中配置创建的对象

（1）Spring配置文件使用xml格式

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
xmlns:xsd="http://www.w3.org/2001/XMLSchema-instance"
xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd">

<bean id="user" class="com.atguigu.spring5.User"></bean>
</beans>
```

6、进行测试代码编写

```java
@Test
public void testAdd(){
	//加载spring配置文件
	ApplicationContext context = new ClassPathXmlApplicationContext("bean.xml");
	//获取配置创建的对象
	User user = context.getBean("user",User.class);
	
	System.out.println(user);
	user.add();
}
```

## 二、IOC容器

### IOC的概念和原理

#### 1、什么是IOC？

（1）控制反转，把对象创建和对象之间的调用过程，交给Spring进行管理

（2）使用IOC目的，为了耦合度降低

（3）做入门案例就是IOC实现

#### 2、IOC底层原理

（1）xml解析

（2）工厂模式

（3）反射

### IOC（接口）

1、IOC思想基于IOC容器完成，IOC容器底层就是对象工厂

2、Spring提供了IOC容器实现的两种方式：（两个接口）

（1）BeanFactory：IOC容器基本实现，是Spring内部的使用接口，不提供开发人员进行使用（加载配置文件时不会创建对象，在获取对象（使用）才去创建对象）

（2）ApplicationContext：BeanFactory接口的子接口，提供更多更强大的功能，一般由开发人员进行使用（加载配置文件时就会把配置文件对象进行创建）

3、ApplicationContext接口的实现类

常用的有FileSystemXmlApplicationContext、ClassPathXmlApplicationContext

### IOC操作Bean管理

**1、什么是Bean管理（指两个操作）**

（1）Spring创建对象

（2）Spring注入属性

**2、Bean管理操作有两种方式**

（1）基于xml配置文件方式实现

（2）基于注解方式实现

### IOC操作Bean管理（基于xml方式）

#### 第1节：基于xml方式进行Bean管理

##### 1、基于xml方式创建对象

```xml
<bean id="user" class="com.jenson.spring.User"></bean>
```

（1）在Spring配置文件中，使用bean标签，标签里面添加对应属性，就可以实现对象创建

（2）在bean标签有很多属性，介绍常用的属性：

​			id属性：唯一标识 

​			class属性：类全路径（包类路径）

（3）创建对象时，默认执行无参构造方法

##### 2、基于xml方式注入属性

（1）DI：依赖注入，就是注入属性

​		**使用set方法进行注入：**

​			1）创建类，定义属性和对应的set方法

```java
public class Book{
	private String bname;
	private String bauthor;
	
	public void setBname(String bname){
		this.bname = bname;
	}
	public void setBauthor(String bauthor){
		this.bauthor = bauthor;
	}
}
```

​			2）在Spring配置文件中注入属性

```xml
<bean id="book" class="com.jenson.spring.Book">
	<property name="bname" value="易筋经"></property>
    <property name="bauthor" value="达摩老祖"></property>
</bean>
```

​		**使用有参构造进行注入：**

​			1）创建类，定义属性，创建属性对应有参数构造方法

```java
public class Orders{
	private String oname;
	private String address;
	public Orders(String oname,String address){
		this.oname = oname;
		this.address = address;
	}
}
```

​			2）在Spring配置文件中进行配置

```xml
<bean id="orders" class="com.jenson.spring.Orders">
	<coustructor-arg name="oname" value="电脑"></constructor-arg>
	<constructor-arg name="address" value="China"></constructor-arg>
</bean>
```

​		**p名称空间注入(了解)**

​			1）使用p名称空间注入，可以简化基于xml配置方式

​					添加p名称空间在配置文件中

```xml
xmlns:p="http://www.springframework.org/schema/p"
```

​					进行属性注入，在bean标签里面进行操作

```xml
<bean id="book" class="com.jenson.spring.Book" p:bname="九阳神功" p:bauthor="无名氏"></bean>
```

#### 第2节：IOC操作Bean管理（xml注入其他类型属性）

##### 1、字面量

（1）null值

```xml
<!--null值-->
<property name="address">
	<null/>
</property>
```

（2）属性值包含特殊符号

```xml
<!--属性指包含特殊符号
	1 把<>进行转义	&lt; &gt;
	2 把带特殊符号内容写到CDATA
-->
<property name="address">
	<value><![CDATA[<<南京>>]]></value>
</property>
```

##### 2、注入属性-外部bean

（1）创建两个类service类和dao类

（2）在service调用dao里面的方法

（3）在Spring配置文件中进行配置

```java
public class UserService{
	//创建UserDao类型属性，生成set方法
	private UserDao userDao;
	public void setUserDao(UserDao userDao){
		this.userDao = userDao;
	}
	
	public void add(){
		System.out.println("service add................");
		userDao.update();
	}
}
```

```xml
<!-- service和dao对象创建-->
<bean id="userService" class="com.jenson.spring.service.UserService">
	<!--注入userDao对象
		name属性：类里面属性名称
		ref属性：创建userDao对象bean标签id值
	-->
	<property name="userDao" ref="userDaoImpl"></property>
</bean>
<bean id="userDaoImpl" class="com.jenson.spring.service.UserDaoImpl"></bean>
```

##### 3、注入属性-内部bean

（1）一对多关系：部门和员工

一个部门有多个员工，一个员工属于一个部门

（2）在实体类之间表示一对多关系，员工表示所属部门，用对象类型属性进行表示。

```java
//部门类
public class Dept{
	prvate String dname;
	public void setDname(String dname){
		this.dname = dname;
	}
}
```

```java
//员工类
public class Emp{
	private String ename;
	private String gender;
    //员工属于某一个部门，使用对象形式表示
    private Dept dept;
    public void setDept(Dept dept){
        this.dept = dept;
    }
	public void setEname(String ename){
		this.ename=ename;	
	}
	public void setGender(String gender){
		this.gender = gender;
	}
}
```

（3）在spring配置文件中进行配置

```xml
<!--内部bean-->
<bean id="emp" class="com.jenson.spring.bean.Emp">
	<!--设置两个普通属性-->
	<property name="ename" value="lucy"></property>
	<property name="gender" value="女"></property>
	<!--设置对象类型属性-->
	<property name="dept">
		<bean id="dept" class="com.jenson.spring.bean.Dept">
			<property name="dname" value="安保部"></property>
		</bean>
	</property>
</bean>
```

##### 4、注入属性-级联赋值

（1）第一种写法

```java
<!--级联赋值-->
<bean id="emp" class="com.jenson.spring.bean.Emp">
	<!--设置两个普通属性-->
	<property name="ename" value="lucy"></property>
	<property name="gender" value="女"></property>
	<!--级联赋值-->
	<property name="dept" ref="dept"></property>
</bean>
<bean id="dept" class="com.jenson.spring.bean.Dept">
	<property name="dname" value="财务部"></property>
</bean>
```

（2）第二种写法(需生成get方法)

```xml
<!--级联赋值-->
<bean id="emp" class="com.jenson.spring.bean.Emp">
	<!--设置两个普通属性-->
	<property name="ename" value="lucy"></property>
	<property name="gender" value="女"></property>
	<!--级联赋值-->
	<property name="dept" ref="dept"></property>
	<property name="dept.dname" value="技术部"></property>
</bean>
<bean id="dept" class="com.jenson.spring.bean.Dept">
	<property name="dname" value="财务部"></property>
</bean>
```

#### 第3节：IOC操作Bean管理（xml注入集合属性）

##### 1、注入集合属性

注入数组、List集合、Map集合、Set集合类型属性

(1)创建类，定义数组、list、map、set类型属性，生成对应set方法

```
public class Stu{
	//数组类型属性
	private String[] courses;
	//list集合类型属性
	private List<String> list;
	//map集合类型属性
	private Map<String,String> maps;
	//set集合类型属性
	private Set<String> sets;
	
	public void setSets(Set<String> sets){
		this.sets = sets;
	}
	public void setCourses(String[] courses){
		this.courses = courses;
	}
	public void setMaps(Map<String,String> maps){
		this.maps = maps;
	}
	public void setList(List<String> list){
		this.list = list;
	}
}
```

（2）在spring配置文件中进行配置

```xml
<!--集合类型属性注入-->
<bean id="stu" class="com.jenson.spring.collectiontype.Stu">
	<!--数组类型属性注入-->
	<property name="courses">
		<array>
			<value>java课程</value>
			<value>数据库课程</value>
		</array>
	</property>
	<!--list类型属性注入-->
	<property name="list">
		<list>
			<value>张三</value>
			<value>小三</value>
		</list>
	</property>
	<!--map类型属性注入-->
	<property name="maps">
		<map>
			<entry key="JAVA" value="java"></entry>
			<entry key="PHP" value="php"></entry>
		</map>
	</property>
	<!--set类型属性注入-->
	<property name="sets">
		<set>
			<value>MySQL</value>
			<value>Redis</value>
		</set>
	</property>
</bean>
```

##### 2、在集合里面设置对象类型值

```xml
<!--创建多个course对象-->
<bean id="course1" class="com.jenson.spring.collectiontype.Course">
	<property name="cname" value="Spring5框架"></property>
</bean>
<bean id="course2" class="com.jenson.spring.collectiontype.Course">
	<property name="cname" value="MyBatis框架"></property>
</bean>

<!--注入list集合类型，值是对象-->
<property name="courseList">
	<list>
		<ref bean="course1"></ref>
		<ref bean="course2"></ref>
    </list>
</property>
```

##### 3、把集合注入部分提取出来

（1）在spring配置文件中引入名称空间util

```xml
命名空间中加入：xmlns:util="http://www.springframework.org/schema/util"
xsi:schemaLocation="http://www.springframework.org/schema/util http://www.springframework.org/schema/util/spring-util.xsd"
```

（2）使用util标签完成list集合注入提取

```xml
<util:list id="bookList">
	<value>易筋经</value>
	<value>九阴真经</value>
	<value>九阳神功</value>
</util:list>
<bean id="book" class="com.atguigu.spring5.collectiontype.Book">
	<property name="list" ref="bookList"></property>
</bean>
```

#### 第4节：IOC操作Bean管理（FactoryBean)

##### 1、Spring有两种类型的bean：

一种是**普通bean**，另外一种是**工厂bean**（FactoryBean）

##### 2、普通bean：

在配置文件中定义bean类型就是返回类型

##### 3、工厂 bean：

在配置文件中定义bean类型可以和返回类型不一样

第一步：创建类，让这个类作为工厂bean，实现接口FactoryBean

第二步：实现接口里面的方法，在实现的方法中定义返回的bean类型

```
public class MyBean implements FactoryBean<Course>{
	//定义返回bean
	@Override
	public Course getObject() throws Exception{
    	Course course = new Course();
    	course.setCname("abc");
    	return course;
	}
	
	@Override
	public Class<?> getObjectType(){
		return null;
	}
	
	@Override
	public boolean isSingleton(){
		return false;
	}
}

<bean id="myBean" class="com.atguigu.spring5.factorybean.MyBean"></bean>

@Test
public void test(){
	ApplicationContext context = new ClassPathXmlApplicationContext("bean.xml");
	Course course = context.getBean("myBean",Course.class);
	System.out.println(course);
}
```

#### 第5节：IOC操作Bean管理（bean作用域）

##### 1、在Spring里面，设置创建bean实例是单实例还是多实例

##### 2、在Spring里面，默认情况下，bean是单实例对象

```java
@Test
public void testCollection2(){
	ApplicationContext context = new ClassPathXmlApplicationContext("bean2.xml");
	Book book1 = context.getBean("book",Book.class);
	Book book2 = context.getBean("book",Book.class);
	//book.test();
	System.out.println(book1);
	System.out.println(book2);
}
```

> 输出结果为：com.jenson.spring.collectiontype.Book@6b12346d
>
> com.jenson.spring.collectiontype.Book@6b12346d
>
> 结果相同，即为同一实例

##### 3、如何设置单实例还是多实例

（1）在Spring配置文件bean标签里面有属性（scope）用于设置单实例还是多实例。

（2）scope属性值

第一个值 默认值，**singleton**，表示是**单实例**对象。

第二个值 **prototype**，表示是**多实例**对象

```xml
<bean id="book" class="com.jenson.spring.collectiontype.Book" scope="prototype">
	<property name="list" ref="bookList"></property>
</bean>
```

（3）singleton和prototype的区别

设置scope值是**singleton**时，**加载spring配置文件时就会创建**单实例对象。

设置scope值是**prototype**时，不是在加载spring配置文件时创建对象，而是**在调用getBean()时创建**多实例对象

#### 第6节：IOC操作Bean管理（bean生命周期）

##### 1、生命周期

从对象创建到对象销毁的过程

##### 2、bean生命周期

（1）通过构造器创建bean实例（无参数构造）

（2）为bean的属性设置值和对其他bean引用（调用set方法）

（3）调用bean的初始化方法（需要进行配置）

（4）bean可以使用了（对象获取到了）

（5）当容器关闭时，调用bean的销毁的方法（需要进行配置销毁的方法）

##### 3、演示生命周期

```java
public class Orders{
	//无参数构造
	public Orders(){
		System.out.println("第一步 执行无参数构造创建bean实例");
	}
    
    private String oname;
    public void setOname(String oname){
        this.oname = oname;
        System.out.println("第二步 调用set方法设置属性值");
    }
    
    //创建执行的初始化的方法
    public void initMethod(){
        System.out.println("第三步 执行初始化的方法");
    }
    //创建执行的销毁的方法
    public void destoryMethod(){
        System.out.println("第五步 执行销毁的方法");
    }
}

@Test
public void testBean(){
	ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("bean.xml");
    Orders orders = context.getBean("orders",Orders.class);
    System.out.println("第四步 获取创建bean实例对象");
    System.out.println(orders);
    
    //手动让bean实例销毁
    context.close();
}
```

```xml
<bean id="orders" class="com.jenson.spring.bean.Orders" init-method="initMethod" destory-method="destoryMethod">
	<property name="oname" value="手机"></property>
</bean>
```

##### 4、bean的后置处理器

加上后置处理器的两步，bean生命周期有7步：

（1）通过构造器创建bean实例（无参数构造）

（2）为bean的属性设置值和对其他bean引用（调用set方法）

**（3）把bean实例传递bean后置处理器的方法postProcessBeforeInitialization**

（4）调用bean的初始化方法（需要进行配置）

**（5）把bean实例传递bean后置处理器的方法postProcessAfterInitialization**

（6）bean可以使用了（对象获取到了）

（7）当容器关闭时，调用bean的销毁的方法（需要进行配置销毁的方法）

##### 5、演示添加后置处理器的效果

（1）创建类，实现接口BeanPostProcessor,创建后置处理器

```java
public class MyBeanPost implements BeanPostProcessor{
	@Override
	public Object postProcessBeforeInitialization(Object bean,String beanName) throws BeansException{
		System.out.println("在初始化之前执行的方法");
		return bean;
	}
	@Override
	public Object postProcessAfterInitialization(Object bean,String beanName) throws BeansException{
		System.out.println("在初始化之后执行的方法");
		return bean;
	}
}
```

（2）配置后置处理器

```xml
<!--配置后置处理器-->
<bean id="myBeanPost" class="com.jenson.spring.bean.MyBeanPost"></bean
```

#### 第7节：IOC操作Bean管理（xml自动装配）

##### 1、什么是自动装配

（1）根据指定装配规则（属性名称或者属性类型），Spring自动将匹配的属性值进行注入

##### 2、演示自动装配过程

（1）根据属性名称自动注入

```xml
<!--实现自动装配
	bean标签属性autowire，配置自动装配
	autowire属性常用两个值：
	byName根据属性名称注入,注入值bean的id值和类属性名称一样 
	byType根据属性类型注入
-->
<bean id="emp" class="com.jenson.spring.autowire.Emp" autowire="byName">
</bean>
<bean id="dept" class="com.jenson.spring.autowire.Dept"></bean>
```

（2）根据属性类型自动注入

```xml
<!--实现自动装配
	bean标签属性autowire，配置自动装配
	autowire属性常用两个值：
	byName根据属性名称注入,注入值bean的id值和类属性名称一样 
	byType根据属性类型注入
-->
<bean id="emp" class="com.jenson.spring.autowire.Emp" autowire="byType">
</bean>
<bean id="dept" class="com.jenson.spring.autowire.Dept"></bean>
```

#### 第8节：IOC操作Bean管理（外部属性文件）

##### 1、直接配置数据库信息

（1）引入德鲁伊连接池依赖jar包

（2）配置德鲁伊连接池

```xml
<!--直接配置连接池-->
<bean id="dataSource" class="com.alibaba.druid.pool.DruidDataSource">
	<property name="driverClassName" value="${jdbc.driverclass}"></property>
	<property name="url" value="${jdbc.url}"></property>
	<property name="username" value="${jdbc.username}"></property>
	<property name="password" value="${jdbc.password}"></property>
</bean>
```

##### 2、引入外部属性文件配置数据库连接池

（1）创建外部属性文件，properties格式文件，写数据库信息

```properties
jdbc.driverClass=com.mysql.jdbc.Driver
jdbc.url=jdbc:mysql://localhost:3306/userDb
jdbc.userName=root
jdbc.password=root
```

（2）把外部properties属性文件引入到spring配置文件中

* 引入context名称空间

```xml
xmlns:context="http://www.springframework.org/schema/context"
xsi:schemaLocation="http://www.springframework.org/schema.org/schema/context http://www.springframework.org/schema/context/spring-context.xsd"
```

* 在spring配置文件中使用context标签引入外部属性文件

```xml
<!--引入外部属性文件-->
<context:property-placeholder location="classpath:jdbc.properties"/>
```

### IOC操作Bean管理（基于注解方式）

#### 1、什么是注解

（1）注解是代码特殊标记，格式：@注解名称（属性名称=属性值，属性名称=属性值）

（2）使用注解，注解作用在类上面，方法上面，属性上面

（3）使用注解的目的，简化xml配置

#### 2、Spring针对Bean管理中创建对象提供注解

（1）@Component

（2）@Service

（3）@Controller

（4）@Repository

上面四个注解功能是一样的，都可以用来创建bean实例

#### 3、基于注解方式实现对象的创建

第一步 引入依赖

spring-aop

第二步 开启组件扫描

引入名称空间context

```xml
<!--开启组件扫描
	1 如果扫描多个包，多个包使用逗号隔开
	2 扫描包上层目录
-->
<context:component-scan base-package="com.jenson.spring.dao,com.jenson.spring.service"></context:component-scan>
```

第三步 创建类，在类上面添加创建对象注解

```java
//在注解里面value属性值可以省略不写
//默认值是类名称，首字母小写
//UserService -- userService
@Component(value="userService")	//<bean id="userService" class=".."/>
public class UserService{
	public void add(){
        System.out.println("service add.......");
    }
}
```

#### 4、开启组件扫描细节配置

```xml
<!--示例1
	use-default-filter="false" 表示现在不使用默认filter，自己配置filter
	context:include-filter,设置扫描哪些内容
-->
<context:component-scan base-package="com.jenson" use-default-filter="false">
	<context:include-filter type="annotation"	expression="org.springframework.stereotype.Controller"/>
</context:component-scan>
 
<!--示例2
	下面配置扫描包所有内容
	context:exclude-filter:设置哪些内容不进行扫描
-->
<context:component-scan base-package="com.jenson">
	<context:exclude-filter type="annotation"	expression="org.springframework.stereotype.Controller"/>
</context:component-scan>
```

#### 5、基于注解方式实现属性注入

##### （1）@AutoWired：根据属性类型进行自动装配

第一步 把service和dao对象创建，在service和dao类添加创建对象注解

第二步 在service注入dao对象

```java
@Service
public class UserService{
	//定义dao类型属性
	//不需要添加set方法
	//添加注入属性注解
	@Autowired		//根据类型进行注入
	private UserDao userDao;
	
	public void add(){
		System.out.println("service add............");
	}
}
```

##### （2）@Qualifier：根据属性名称进行注入

这个@Qualifier注解的使用，要和上面@Autowired一起使用

```java
@Service
public class UserService{
	//定义dao类型属性
	//不需要添加set方法
	//添加注入属性注解
	@Autowired
    @Qualifier(value="userDaoImpl")	//根据名称进行注入
	private UserDao userDao;
	
	public void add(){
		System.out.println("service add............");
	}
}
```

##### （3）@Resource：可以根据类型注入，可以根据名称注入

```java
//@Resource //根据类型进行注入
@Resource(name="userDaoImpl")	//根据名称进行注入
private UserDao userDao;
```

##### （4）@Value：注入普通类型属性

```java
@Value(value="abc")
private String name;
```

#### 6、完全注解开发

（1）创建配置类，替代xml配置文件

```java
@Configuration 		//作为配置类，替代xml配置文件
@ComponentScan(basePackages={"com.atguigu"})
public class SpringConfig{
	
}
```

（2）编写测试类

```java
@Test
public void testService2(){
	//加载配置类
	ApplicationContext context = new AnnotationConfigApplicationContext(SpringConfig.class);
	UserService userService = context.getBean("userService",UserService.class);
	System.out.println(userService);
	userService.add();
}
```

## 三、AOP

### AOP（概念）

（1）面向切面编程（方面），利用AOP可以**对业务逻辑的各个业务进行隔离**，从而使得业务逻辑各部分之间的**耦合度降低**，**提高**程序的**可重用性**，同时提高了开发的效率。

（2）通俗描述：不通过修改源代码方式，在主干功能里面添加新功能

（3）使用登录例子说明AOP

![](D:\Java\blog\source\_posts\MySQL45讲\AOP登录功能例子.png)

### AOP（动态代理）

#### AOP底层原理

**AOP底层使用动态代理**

**有两种情况的动态代理**

**第一种**： 有接口情况，使用JDK动态代理

创建接口实现类代理对象，增强类的方法

![](D:\Java\blog\source\_posts\MySQL45讲\动态代理有接口.png)

**第二种**： 没有接口情况，使用CGLIB动态代理

创建子类的代理对象，增强类的方法

![](D:\Java\blog\source\_posts\MySQL45讲\动态代理没有接口.png)

#### AOP（JDK动态代理）

1、使用JDK动态代理，使用Proxy类里面的方法创建代理对象

**调用newProxyInstance**方法，**方法有三个参数**：

1. 类加载器，
2. 增强方法所在的类，这个类实现的接口，支持多个接口，
3. 实现这个接口**InvocationHandler**，创建代理对象，写增强的方法

2、编写JDK动态代理代码

（1）创建接口，定义方法

```java
public interface UserDao{
	public int add(int a,int b);
	public String update(String id);
}
```

（2）创建接口实现类，实现方法

```java
public class UserDaoImpl implements UserDao{
	@Override
	public int add(int a,int b){
		return a+b;
	}
	@Override
	public String update(String id){
		return id;
	}
}
```

（3）使用Proxy类创建接口代理对象

```java
public class JDKProxy{
	public static void main(String[] args){
		//创建接口实现类代理对象
		Class[] interfaces = {UserDao.class};
		UserDaoImpl userDao = new UserDaoImpl();	UserDao dao = (UserDao)Proxy.newProxyInstance(JDKProxy.class.getClassLoader(),interfaces,new UserDaoProxy(userDao));
        int result = dao.add(1,2);
        System.out.println("result:"+result);
	}
}

//创建代理对象代码
class UserDaoProxy implements InvocationHandler{
	//创建的是谁的代理对象，就把谁传递过来
	//有参数构造传递
	private Object obj;
	public UserDaoProxy(Object obj){
		this.obj = obj;
	}
	
	//增强的逻辑
	@Override
	public Object invoke(Object proxy,Method method,Object[] args) throws Throwable{
		//方法之前
		System.out.println("方法之前执行...."+method.getName()+":传递的参数..."+Arrays.toString(args));
		
		//被增强的方法执行
		Object res = method.invoke(obj,args);
		
		//方法之后
		System.out.println("方法之后执行..."+obj);
		return res;
	}
}
```

### AOP（术语）

#### 1、连接点

类里面的哪些方法可以被增强，这些方法称为连接点

#### 2、切入点

实际被真正增强的方法，称为切入点。

#### 3、通知（增强）

##### （1）实际增强的逻辑部分称为通知（增强）

##### （2）通知有多种类型：

* 前置通知(before)
* 后置通知(AfterReturning)也叫返回通知（有异常则不会执行）
* 环绕通知(around)
* 异常通知(AfterThrowing)
* 最终通知(After)无论有无异常都会执行

#### 4、切面（是一个动作）

 把通知应用到切入点过程

### AOP操作（准备）

1、Spring框架一般都是基于AspectJ实现AOP操作

AspectJ不是Spring组成部分，独立AOP框架，一般把AspectJ和Spring框架一起使用，进行AOP操作

2、基于AspectJ实现AOP操作

（1）基于xml配置文件实现

（2）基于注解方式实现（常用）

3、在项目工程中引入AOP相关依赖

com.springsource.net.sf.cglib，com.springsource.org.aopalliance，com.springsource.org.aspectj.weaver，commons-logging，druid，spring-aop，spring-aspects，spring-beans，spring-context，spring-core，spring-expression

4、切入点表达式

（1）切入点表达式作用：知道对哪个类里面的哪个方法进行增强

（2）语法结构：

execution([权限修饰符] [返回类型] [类全路径] [方法名称] ([参数列表]))

举例1：对com.jenson.dao.BookDao类里面的add进行增强

execution(* com.jenson.dao.BookDao.add(..))

举例2：对com.jenson.dao.BookDao类里面的所有的方法进行增强

execution(* com.jenson.dao.BookDao.*(..))

举例3：对com.jenson.dao包里面所有类，类里面所有方法进行增强

execution(* com.jenson.dao.*. *(..))

### AOP操作（AspectJ注解）常用

#### 1、创建类，在类里面定义方法

```java
public class User{
	public void add(){
		System.out.println("add.......");
	}
}
```

#### 2、创建增强类（编写增强逻辑）

（1）在增强类里面，创建方法，让不同方法代表不同通知类型

```java
//增强的类
public class UserProxy{
	//前置通知
	public void before(){
		System.out.println("before.......");
	}
}
```

#### 3、进行通知的配置

（1）在spring配置文件中，开启注解扫描

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
xmlns:context="http://www.springframework.org/schema/context"
xmlns:aop="http://www.springframework.org/schema/aop"
xsi:schemaLocation="http://www.springframework.org/schema/beans http://springframework.org/schema/beans/spring-beans.xsd
http://www.springframework.org/schema/context http://www.springframework.org/schema/context/spring-context.xsd
http://www.springframework.org/schema/aop http://www.springframework.org/schema/aop/spring-aop.xsd">
	<!--开启注解扫描-->
	<context:component-scan base-package="com.jenson.spring.aopanno"></context:component-scan>
```



（2）使用注解创建User和UserProxy对象

```java
//被增强的类
@Component
public class User{
//增强的类
@Component
public class UserProxy{
```

（3）在增强类上面添加注解@Aspect

```java
//增强的类
@Component
@Aspect				//生成代理对象
public class UserProxy{
```

（4）在spring配置文件中开启生成代理对象

```xml
<!--开启Aspect生成代理对象-->
<aop:aspectj-autoproxy></aop:aspectj-autoproxy>
```

#### 4、配置不同类型的通知

（1）在增强类的里面，在作为通知方法上面添加通知类型注解，使用切入点表达式配置

```java
//增强的类
@Component
@Aspect      //生成代理对象
public class UserProxy{
    
    //相同切入点抽取
    @Pointcut(value="execution(* com.jenson.spring.aopanno.User.add(..))")
    public void pointdemo(){
        
    }
    
	//前置通知
	//@Before直接表示作为前置通知
	@Before(value="pointdemo()")
	public void before(){
    	System.out.println("before..........");
	}
    
    //后置通知（返回通知）
    @AfterReturning(value="execution(* com.jenson.spring.aopanno.User.add(..))")
    public void afterReturning(){
        System.out.println("afterReturning.......");
    }
    
    //最终通知
    @After(value="execution(* com.jenson.spring.aopanno.User.add(..))")
    public void after(){
        System.out.println("after...........");
    }
    
    //异常通知
    @AfterThrowing(value="execution(* com.jenson.spring.aopanno.User.add(..))")
    public void afterThrowing(){
        System.out.println("afterThrowing......");
    }
    
    //环绕通知
    @Around(value="execution(* com.jenson.spring.aopanno.User.add(..))")
    public void around(ProceedingJoinPoint) throws Throwable{
        System.out.println("环绕之前.........");
        
        //被增强的方法执行
        proceedingJoinPoint.proceed();
        
        System.out.println("环绕之后........");
    }
}
```

#### 5、相同的切入点抽取

```java
//相同切入点抽取
    @Pointcut(value="execution(* com.jenson.spring.aopanno.User.add(..))")
    public void pointdemo(){
        
    }
    
	//前置通知
	//@Before直接表示作为前置通知
	@Before(value="pointdemo()")
	public void before(){
    	System.out.println("before..........");
	}
```

#### 6、有多个增强类对同一个方法进行增强，设置增强类优先级

在增强类上面添加注解**@Order(数字类型值)**，该值越小，优先级越高

```java
@Component
@Aspect
@Order(1)
public class PersonProxy{
```

#### 7、完全使用注解开发

（1）创建配置类，不需要创建xml配置文件

```java
@Configuration
@ComponentScan(basePackages={"com.jenson"})
@EnableAspectJAutoProxy(proxyTargetClass=true)
public class ConfigAop{
}
```

### AOP操作（AspectJ配置文件）了解

1、创建两个类，增强类和被增强类，创建方法

2、在spring配置文件中创建两个类对象

```xml
<!--创建对象-->
<bean id="book" class="com.jenson.spring.aopxml.Book"></bean>
<bean id="bookProxy" class="com.jenson.spring.aopxml.BookProxy"></bean>
```

3、在spring配置文件中配置切入点

```xml
<!--配置aop增强-->
<aop:config>
	<!--切入点-->
	<aop:pointcut id="p" expression="execution(* com.jenson.spring.aopxml.Book.buy(..))"/>
	<!--配置切面-->
	<aop:aspect ref="bookProxy">
		<!--增强作用在具体的方法上-->
		<aop:before method="before" pointcut-ref="p"/>
	</aop:aspect>
<aop:config>
```

## 四、JdbcTemplate

**概念：**Spring框架对JDBC进行封装，使用JdbcTemplate方便实现对数据库操作

### 准备工作

（1）引入依赖的jar包

druid，mysql-connector-java，spring-jdbc，spring-orm，spring-tx

（2）在spring配置文件中配置数据库连接池

```xml
<bean id="dataSource" class="com.alibaba.druid.pool.DruidDataSource" destroy-method="close">
	<property name="url" value="jdbc:mysql:///user_db"/>
	<property name="username" value="root"/>
	<property name="password" value="root"/>
	<property name="driverClassName" value="com.mysql.jdbc.Driver"/>
</bean>
```

（3）配置jdbcTemplate对象：注入DataSource

```xml
<!--JdbcTemplate对象-->
<bean id="jdbcTemplate" class="org.springframework.jdbc.core.JdbcTemplate">
	<!--注入dataSource-->
	<property name="dataSource" ref="dataSource"></property>
</bean>
```

（4）创建service类，创建dao类，在dao注入jdbcTemplate对象

```xml
<!--组件扫描-->
<context:component-scan base-package="com.jenson"></context:component-scan>
```

```java
@Service
public class BookService{
	//注入dao
	@Autowired
	private BookDao bookDao;
}
```

```java
@Repository
public class BookDaoImpl implements BookDao{
	//注入JdbcTemplate
	@Autowired
	private JdbcTemplate jdbcTemplate;
}
```

### JdbcTemplate操作数据库

#### 添加

##### 1、对应数据库创建实体类

```java
public class User{
	private String userId;
	private String username;
	private String ustatus;
	
	public void setUserId(String userId){
		this.userId = userId;
	}
	public void setUsername(String username){
		this.username = username;
	}
	public void setUstatus(String ustatus){
		this.ustatus = ustatus;
	}
}
```

##### 2、编写service和dao

（1）在dao进行数据库添加操作

（2）调用JdbcTemplate对象里面的update方法实现添加操作。

```java
update(String sql,Object... args)		//有两个参数，第一个参数：sql语句；第二个参数：可变参数，设置sql语句值。
```

```java
@Repository
public class BookDaoImpl implements BookDao{
	//注入JdbcTemplate
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	//添加的方法
	@Override
	public void add(Book book){
		//创建sql语句
		String sql = "insert into t_book values(?,?,?)";
		//调用方法实现
		Object[] args = {book.getUserId(),book,getUsername(),book.getUstatus()};
		int update = jdbcTemplate.update(sql,args);
		System.out.println(update);
	}
}
```

##### 3、测试类

```java
public void testJdbcTemplate(){
	ApplicationContext context = new ClassPathXmlApplicationContext("bean1.xml");
	BookService bookService = context.getBean("bookService",BookService.class);
	
	Book book = new Book();
	book.setUserId("1");
	book.setUsername("java");
	book.setUstatus("a");
	bookService.addBook(book);
}
```

#### 修改和删除

```java
//修改
@Override
public void updateBook(Book book){
	String sql = "update t_book set username=?,ustatus=? where user_id=?";
	Object[] args = {book.getUsername(),book.getUstatus(),book.getUserId()};
	int update = jdbcTemplate.update(sql,args);
	System.out.println(update);
}

//删除
@Override
public void delete(String id){
	String sql = "delete from t_book where user_id=?";
	int update = jdbcTemplate.update(sql,id);
	System.out.println(update);
}
```

#### 查询返回某个值

1、查询表里面有多少条记录，返回是某个值

2、使用JdbcTemplate实现查询返回某一个值

```java
quertForObject(String sql,Class<T> requiredType)//第一个参数：sql语言；第二个参数：返回类型Class
```

```java
//查询表记录数
@Override
public int selectCount(){
	String sql = "select count(*) from t_book";
	Integer count = jdbcTemplate.queryForObject(sql,Integer.class);
	return count;
}
```

#### 查询返回对象

1、场景：查询图书详情

2、JDBCTemplate实现查询返回对象

```java
queryForObject(String sql,RowMapper<T> rowMapper,Object... args)//第一个参数：sql语句；第二个参数：RowMapper，是接口，针对返回不同类型数据，使用这个接口里面实现类完成数据封装；第三个参数：sql语句值
```

```java
//查询返回对象
@Override
public Book findBookInfo(String id){
	String sql = "select * from t_book where user_id=?";
	//调用方法
	Book book = jdbcTemplate.queryForObject(sql,new BeanPropertyRowMapper<Book>(Book.class),id);
	return book;
}
```

#### 查询返回集合

1、场景：查询图书列表分页

2、调用JdbcTemplate方法实现查询返回集合

```java
query(String sql,RowMapper<T> rowMapper,Object... args)////第一个参数：sql语句；第二个参数：RowMapper，是接口，针对返回不同类型数据，使用这个接口里面实现类完成数据封装；第三个参数：sql语句值
```

```java
public List<Book> findAllBook(){
	String sql = "select * from t_book";
	//调用方法
	List<Book> bookList = jdbcTemplate.query(sql,new BeanPropertyRowMapper<Book>(Book.class));
	return bookList;
}
```

#### 批量操作

1、批量操作，操作表里多条记录

2、JdbcTemplate实现批量添加操作

```java
batchUpdate(String sql,List<Object[]> batchArgs)//第一个参数：sql语句；第二个参数：List集合，添加多条记录数据
//批量添加
@Override
public void batchAddBook(List<Object[]> batchArgs){
    String sql = "insert into t_book values(?,?,?)";
    int[] ints = jdbcTemplate.batchUpdate(sql,batchArgs);
    System.out.println(Arrays.toString(ints));
}
```

3、JdbcTemplate实现批量修改操作

```java
//批量修改
@Override
public void batchUpdateBook(List<Object[]> batchArgs){
	String sql = "update t_book set username=?,ustatus=? where user_id=?";
	int[] ints = jdbcTemplate.batchUpdate(sql,batchArgs);
	System.out.println(Arrays.toString(ints));
}
```

4、JdbcTemplate实现批量删除操作

```java
//Dao中代码
@Override
public void batchDeleteBook(List<Object[]> batchArgs){
	String sql = "delete from t_book where user_id=?";
	int[] ints = jdbcTemplate.batchUpdate(sql,batchArgs);
	System.out.println(Arrays.toString(ints));
}
//批量删除(测试类中代码)
List<Object[]> batchArgs = new ArrayList<>();
Object[] o1 = {"3"};
Object[] o2 = {"4"};
batchArgs.add(o1);
batchArgs.add(o2);
//调用方法实现批量修改
bookService.batchDelete(batchArgs);
```

## 五、事务

### 事务的概念

1、什么是事务

（1）事务是数据库操作最基本单元，逻辑上一组操作，要么都成功，如果有一个失败，所有操作都失败。

（2）典型场景：银行转账。

2、事务四个特性（ACID）

（1）原子性

（2）一致性

（3）隔离性

（4）持久性

### 事务操作（搭建事务操作环境）

1、创建数据库表，添加记录

| id   | username | money |
| ---- | -------- | ----- |
| 1    | lucy     | 1000  |
| 2    | mary     | 1000  |

2、创建service，搭建dao，完成对象创建和注入关系。

（1）service注入dao，在dao注入JdbcTemplate，在JdbcTemplate注入DataSource

```java
@Service
public class UserService{
	//注入dao
	@Autowired
	private UserDao userDao;
}
@Resitory
public class UserDaoImpl implements UserDao{
	@Autowired
	private JdbcTemplate jdbcTemplate;
}
```

（2）在dao创建两个方法：多钱和少钱的方法，在service创建方法（转账的方法）

```java
@Resitory
public class UserDaoImpl implements UserDao{
	@Autowired
	private JdbcTemplate jdbcTemplate;
    
    //lucy转账100给mary
    //少钱
    @Override
    public void reduceMoney(){
        String sql = "update t_account set money=money-? where username=?";
        jdbcTemplate.update(sql,100,"lucy")
    }
    
    //多钱
    @Override
    public void addMoney(){
        String sql = "update t_account set money=money+? where username=?";
        jdbcTemplate.update(sql,100,"mary");
    }
}
```

```java
@Service
public class UserService{
	//注入dao
	@Autowired
	private UserDao userDao;
	
	//转账的方法
	public void accountMoney(){
		//lucy少100
		userDao.reduceMoney();
		
		//mary多100
		userDao.addMoney();
	}
}
```

4、上面代码，如果正常执行没有问题，但是如果代码执行过程中出现异常，有问题，此时可使用事务进行解决

```java
//转账的方法
public void accountMoney(){
	//lucy少100
	userDao.reduceMoney();
	
	//模拟异常
	int i = 10/0;
	
	//mary多100
	userDao.addMoney();
}
```

事务操作过程

```java
public void accountMoney(){
    try{
        //第一步 开启事务
        
       	//第二步 进行业务操作
        //lucy少100
		userDao.reduceMoney();
	
		//模拟异常
		int i = 10/0;
	
		//mary多100
		userDao.addMoney();
        
        //第三步 没有发生异常，提交事务
    }catch(Exception e){
        //第四步 出现异常，事务回滚
    }
}
```

事务操作（Spring事务管理介绍）

1、事务添加到JavaEE三层结构里面的Service层（业务逻辑层）

2、在Spring进行事务管理操作

（1）有两种方式，编程式事务管理和声明式事务管理（使用）

3、声明式事务管理

（1）基于注解方式（使用）

（2）基于xml配置文件方式

4、在Spring进行声明式事务管理，底层使用AOP

5、Spring事务管理API

（1）提供一个接口，代表事务管理器，这个接口针对不同的框架提供不同的实现类

### 事务操作（注解声明式事务管理）

1、在spring配置文件配置事务管理器

```java
<!--创建事务管理器-->
<bean id="transactionManager" class="org.springframework.jdbc.datasource.DataSourceTransactionManager">
	<!--注入数据源-->
	<property name="dataSource" ref="dataSource"></property>
</bean>
```

2、在spring配置文件，开启事务注解

（1）在spring配置文件引入名称空间tx

```xml
<beans xmlns="http://www.springframework.org/schema/beans"
	   xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	   xmlns:context="http://www.springframework.org/schema/context"
	   xmlns:aop="http://www.springframework.org/schema/aop"
	   xmlns:tx="http://www.springframwork.org/schema/tx"
	   xsi:schemaLocation="http://www.springframwork.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd
	   http://www.springframework.org/schema/context http://www.springframework.org/schema/context/spring-context.xsd
	   http://www.springframework.org/schema/aop http://www.springframework.org/schema/aop/spring-aop.xsd
	   http://www.springframework.org/schema/tx http://www.springframework.org/schema/tx/spring-tx.xsd">
```

（2）开启事务注解

```xml
<!--开启事务注解-->
<tx:anntation-driven transaction-manager="transactionManager"></tx:annotation-driver>
```

3、在service类上面（获取service类里面方法上面）添加事务注解

（1）@Transactional，这个注解添加到类上面，也可以添加到方法上面

（2）如果把这个注解添加类上面，这个类里面所有的方法都添加事务

（3）如果把这个注解添加方法上面，为这个方法添加事务

```java
@Service
@Transactional
public class UserService{
}
```

#### 声明式事务管理参数配置

在service类上面添加注解@Transactional，在这个注解里面可以配置事务相关参数。

![](C:\Users\Dongjuncheng\Desktop\事务管理参数配置.png)

##### 1、propagation：事务传播行为

（1）多事务方法直接进行调用，这个过程中事务是如何进行管理的

事务方法：对数据库表数据进行变化的操作

Spring框架事务传播行为有7种：

REQUIRED（默认）：如果add方法本身有事务，调用update方法之后，update使用当前add方法里面事务；如果add方法本身没有事务，调用update方法之后，创建新事务。

REQUIRED_NEW：使用add方法调用update方法，如果add无论是否有事务，都创建新的事务。

SUPPORTS：如果有事务在运行，当前的方法就在这个事务内运行，否则它可以不运行的事务中。

NOT_SUPPORTED：当前的方法不应该运行在事务中，如果有运行的事务，将它挂起。

MANDATORY：当前的方法不应该运行在事务中，如果有运行的事务，就抛出异常

NESTED：如果有事务在运行，当前的方法就应该在这个事务的嵌套事务内运行，否则，就启动一个新的事务，并在它自己的事务内运行。

```java
@Service
@Transaction(propagation = Propagation.REQUIRED)
public class UserService{}
```

##### 2、ioslation：事务隔离级别

（1）事务有特性成为隔离性，多事务操作之间不会产生影响，不考虑隔离性产生很多问题。

（2）有三个读问题：脏读、不可重复读、幻读

（3）脏读：一个未提交事务读取到另一个未提交事务的数据。

（4）不可重复读：一个未提交事务读取到另一个提交事务修改数据。

（5）虚（幻）读：一个未提交事务读取到另一提交事务添加数据。

（6）通过设置事务隔离级别，解决读问题

|                            | 脏读 | 不可重复读 | 幻读 |
| -------------------------- | ---- | ---------- | ---- |
| READ UNCOMMITTED(读未提交) | 有   | 有         | 有   |
| READ COMMITTED(读已提交)   | 无   | 有         | 有   |
| REPEATABLE READ(可重复读)  | 无   | 无         | 有   |
| SERIALIZABLE(串行化)       | 无   | 无         | 无   |

```java
@Service
@Transactional(propagation=Progapation.REQUIRED,isolation=Isolation.REPEATABLE_READ)
public class UserService{}
```

##### 3、timeout：超时时间

（1）事务需要在一定时间内进行提交，如果不提交进行回滚

（2）默认值是-1，设置时间以秒单位进行计算

##### 4、readOnly：是否只读

（1）读：查询操作，写：添加修改删除操作

（2）readOnly默认值false，表示可以查询，可以添加修改删除操作

（3）设置readOnly默认值是true，设置成true之后，只能查询

##### 5、rollbackFor：回滚

（1）设置出现哪些异常进行事务回滚

##### 6、noRollbackFor：不回滚

（1）设置出现哪些异常不进行事务回滚

### 事务操作（XML声明式事务管理）

1、在spring配置文件中进行配置

第一步 配置事务管理器

第二步 配置通知

第三步 配置切入点和切面

```xml
<!--1 创建事务管理器-->
<bean id="transactionManager" class="org.springframework.jdbc.datasource.DataSourceTransactionManager">
	<!--注入数据源-->
	<property name="dataSource" ref="dataSource"></property>
</bean>

<!--2 配置通知-->
<tx:advice id="txadvice">
	<!--配置事务参数-->
	<tx:attributes>
		<!--指定哪种规则的方法上面添加事务-->
		<tx:method name="accountMoney" propagation="REQUIRED"/>
		<!--<tx:method name="account*"/>-->
	</tx:attributes>
</tx:advice>

<!--3 配置切入点和切面-->
<aop:config>
	<!--配置切入点-->
	<aop:pointcut id="pt" expression="execution(* com.jenson.spring.service.UserService.*(..))"/>
	<!--配置切面-->
	<aop:advisor advice-ref="txadvice" pointcut-ref="pt"/>
</aop:config>
```

### 事务操作（完全注解声明式事务管理）

1、创建配置类，使用配置类

```java
@Configuration //配置类
@ComponentScan(basePackages="com.jenson")	//组件扫描
@EnableTransactionManagement	//开启事务
public class TxConfig{
	//创建数据库连接池
	@Bean
	public DruidDataSource getDruidDataSource(){
		DruidDataSource DruidDataSource = new DruidDataSource();
		dataSource.setDriverClassName("com.mysql.jdbc.Driver");
		dataSource.setUrl("jdbc:mysql:///user_db");
		dataSource.setUsername("root");
		dataSource.setPassword("root");
		return dataSource;
	}
    
    //创建jdbcTemplate对象
    @Bean
    public JdbcTemplate getJdbcTemplate(DataSource dataSource){
        //到ioc容器中根据类型找到dataSource
        JdbcTemplate jdbcTemplate = new JdbcTemplate();
        //注入dataSource
        jdbcTemplate.setDataSource(dataSource);
        return jdbcTemplate;
    }
    
    //创建事务管理器
    @Bean
    public DataSourceTransactionManager getDataSourceTransactionManager(DataSource dataSource){
        DataSourceTransactionManager transactionManager = new DataSourceTransactionManager();
        transactionManager.setDataSource(dataSource);
        return transactionManager;
    }
}
```

## 六、Spring5.0新功能

1、整个框架的代码基于Java8，运行时兼容JDK9，许多不建议使用的类和方法在代码库中删除。

2、Spring5.0框架自带了通用的日志封装

（1）Spring5已经移除了Log4jConfigListener,官方建议使用Log4j2

（2）Spring框架整合了Log4j2

​	第一步：引入jar包

​	第二步：创建log4j2.xml配置文件

```xml
<?xml version="1.0" encoding="UTF-8"?>
<!--日志级别以及优先级排序：OFF>FATAL>ERROR>WARN>INFO>DEBUG>TRACE>ALL-->
<!--Configuration后面的status用于设置log4j2自身内部的信息输出，可以不设置，当设置成trace时，可以看到log4j2内部各种详细输出-->
<configuration status="INFO">
	<!--先定义所有的appender-->
	<appenders>
	<!--输出日志信息到控制台-->
	<console name="Console" target="SYSTEM_OUT">
		<!_-控制日志输出的格式-->
		<PatternLayout pattern="%d{yyyy-MM-dd HH:mm:ss.SSS} [%t] %-5level%logger{36}-%msg%n"/>
    </console>
    </appenders>
    <!--然后定义logger，只有定义了logger并引入的appender，appender才会生效-->
    <!--root：用于指定项目的根日志，如果没有单独指定Logger，则会使用root作为默认的日志输出-->
    <logggers>
    	<root level="info">
        	<appender-ref ref="Console"/>
        </root>
    </logggers>
</configuration>
```

3、Spring5框架核心容器支持@Nullable注解

（1）@Nullable注解可以使用在方法上面，属性上面，参数上面，表示方法返回可以为空，属性值可以为空，参数值可以为空。

（2）注解用在方法上面，方法返回值可以为空

```java
@Nullable
String getId();
```

（3）注解使用在方法参数里面，方法参数可以为空

```java
public <T> void registerBean(@Nullable String beanName,Class<T> beanClass,@Nullable Supplier<T> supplier){
	this.reader.registerBean(beanClass,beanName,supplier,customizers);
}
```

（4）注解使用在属性上面，属性值可以为空

```java
@Nullable
private String bookName;
```

4、Spring5核心容器支持函数式风格GenericApplicationContext

```java
//函数式风格创建对象，交给spring进行管理
@Test
public void testGenericApplicationContext(){
	//创建GenericApplicationContext对象
	GenericApplicationContext context = new GenericApplicationContext();
	//调用context的方法对象注册
	context.refresh();
	context.registerBean("user1",User.class,() -> new User());
	//获取在spring注册的对象
	//User user = (User)context.getBean("com.jenson.spring.test.User");
    User user = (User)context.getBean("user1");
    System.out.println(user);
}
```

5、Spring5支持整合JUnit5

（1）整合JUnit4

第一步 引入Spring相关针对测试的依赖

第二步 创建测试类

```java
@RunWith(SpringJUnit4ClassRunner.class)//单元测试框架
@ContextConfiguration("classpath:bean1.xml")//加载配置文件
public class JTest4{
	@Autowired
	private UserService userService;
	
	@Test
	public void test1(){
		userService.accountMoney();
	}
}
```

（2）Spring5整合JUnit5

第一步 引入JUnit5的jar包

第二步 创建测试类，使用注解完成

```java
@ExtendWith(SpringExtension.class)
@ContextConfiguration("classpath:bean1.xml")
public class JTest5{
	@Autowired
	private UserService userService;
	
	@Test
	public void test1(){
		userService accountMoney();
	}
}
```

（3）使用一个复合注解替代上面两个注解完成整合

```java
@SpringJUnitConfig(locations="classpath:bean1.xml")
public class JTest5{
	@Autowired
	private UserService userService;
	
	@Test
	public void test1(){
		userService.accountMoney();
	}
}
```

#### SpringWebflux

1、SpringWebflux介绍

（1）是Spring5添加新的模块，用于web开发的，功能和SpringMVC类似的，Webflux使用当前一种比较流行的响应式编程出现的框架。

（2）使用传统web框架，比如SpringMVC，这些基于Servlet容器，**Webflux是一种异步非阻塞的框架**，异步非阻塞的框架在Servlet3.1以后才支持，核心是基于Reactor的相关API实现的。

（3）什么是异步非阻塞

**异步和同步针对调用者**，调用者发送请求，如果等着对方回应之后才去做其他事情就是同步，如果发送请求之后不等着对方回应就去做其他事情就是异步。

**非阻塞和阻塞针对被调用者**，被调用者收到请求之后，做完请求任务之后才给出反馈就是阻塞，收到请求之后马上给出反馈然后再去做事情就是非阻塞。

（4）Webflux特点：

* 非阻塞式：在有限资源下，提高系统吞吐量和伸缩性，以Reactor为基础实现响应式编程
* 函数式编程：Spring5框架基于java8，Webflux可以使用java8函数式编程方式实现路由请求。

（5）比较SpringMVC

第一 两个框架都可以使用注解方式，都运行在Tomcat等容器中

第二 SpringMVC采用命令式编程，Webflux采用异步响应式编程。

2、响应式编程

（1）什么是响应式编程

响应式编程是一种面向数据流和变化传播的编程范式，这意味着可以在编程语言中很方便地表达静态或动态地数据流，而相关的计算模型会自动将变化的值通过数据流进行传播。

（2）Java8以及之前版本

* 提供的观察者模式两个类Observer和Observable

```java
public class ObserverDemo extends Observable{
	public static void main(String[] args){
		ObserverDemo observer = new ObserverDemo();
		//添加观察者
		observer.addObserver((o,arg)->{
			System.out.println("发生变化");
		});
		observer.addObserver((o,arg)->{
			System.out.println("手动被观察者通知，准备改变");
		});
		observer.setChanged();//数据变化
		observer.notifyObservers();//通知
	}
}
```

3、Webflux执行流程和核心API

4、SpringWebflux（基于注解编程模型）

5、SpringWebflux（基于函数式编程模型）

