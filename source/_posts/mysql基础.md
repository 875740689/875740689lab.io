---
title: mysql基础
date: 2020-09-23 21:08:51
tags: MySQL
---

### 单行函数

###### 一、字符函数

**1、length	获取长度**

SELECT LENGTH('john');

**2、concat	拼接字符串**

SELECT CONCAT(last_name,'_',first_name) 姓名 FROM emplyees<!--more-->

**3、upper、lower	大小写转换**

SELECT UPPER('john');

SELECT LOWER('joHN');

**4、substr、substring**

注意：索引从1开始

#截取从指定索引处后面所有字符

SELECT SUBSTR('李莫愁爱上了陆湛远',6)  out_put;

#截取从指定索引处指定字符长度的字符

SELECT SUBSTR('李莫愁爱上了陆湛远',1,3)  out_put;

#案例：姓名中首字母大写，其他字母小写然后用_拼接，显示出来

SELECT CONCAT(UPPER(SUBSTR(last_name,1,1),'_',LOWER(SUBSTR(last_name,2)))  out_put FROM employees;

**5、instr**		返回子串第一次出现的索引，如果找不到返回0

SELECT INSTR('眼不会爱上了因留下','因留下') AS out_put FROM employees;

**6、trim**	去空格

SELECT LENGTH(TRIM('	张吹上	')) AS out_put;

去掉字符串前后的a

SELECT TRIM('a' FROM 'aaaaaaaaaaaaaaa张aaaaaaaaaaaa吹上aaaaaaaaaaaaaa') AS out_put；

**7、lpad**		用指定的字符实现左填充指定长度

SELECT LPAD('殷素素',10,'*') AS out_put;

**8、rpad** 		用指定的字符实现右填充指定长度

**9、replace**		替换

SELECT REPLACE('周至柔爱上张无忌周芷若爱上了张无忌爱上了周芷若'，'周芷若','赵敏') AS out_put;

###### 二、数学函数

**1、round**	四舍五入

SELECT ROUND(1.65);

**2、ceil**	向上取整

SELECT CEIL(1.52);

**3、floor**	向下取整

SELECT FLOOR(-9.99);

**4、truncate**	截断(小数点后保留几位)

SELECT TRUNCATE(1.65,1);

**5、mod**	取余(%)

SELECT MOD(-10,-3) ;		结果为-1

###### 三、日期函数

**1、now**	返回当前系统日期+时间

SELECT NOW();

**2、curdate**	返回当前系统日期，不包含时间

**3、curtime**	返回当前系统时间，不包含日期

**4、获取指定的部分**	SELECT YEAR('1998-1-1') AS 年；

SELECT MOUTH(NOW()) AS 月;

**5、str_to_date**	将字符通过指定格式转换为日期

SELECT STR_TO_DATE('1998-3-2','%Y-%c-%d');

**6、date_format**	将日期转换成字符

SELECT DATE_FORMAT(NOW(),'%y年%m月%d日') AS out_put;

###### 四、其他函数

SELECT VERSION();

SELECT DATABASE();

SELECT USER();

###### 五、流程控制函数

**1、if函数**： if else的效果

SELECT IF(10<5,'大','小');

**2、case函数**： 

（1）switch case的效果

case 要判断的字段或表达式

when 常量1 then 要显示的值1或语句1

when 常量2 then 要显示的值2或语句2

...

else 要显示的值n或语句n

end

（2）多重if的效果

case

when 条件1 then 要显示的值1或语句1

when 条件2 then 要显示的值2或语句2

...

else 要显示的值n或语句n

end

### 分组函数

功能:用作统计使用，又称为聚合函数或统计函数或组函数

分类：sum求和、avg平均值、max最大值、count计算个数

简单的使用

SELECT SUM(salary) FROM employees;

SELECT AVG(salary) FROM employees;

特点：

1、sum、avg一般用于处理数值型

max、min、count可以处理任何类型

2、以上分组函数都忽略null值

3、可以和distinct搭配实现去重的运算

4、count函数的详细介绍

SELECT COUNT(sarary) FROM employees;

SELECT COUNT(*) FROM employees;	连值为null的也统计起来

5、和分组函数一同查询的字段有限制



**GROUP BY**子句将表中的数据分成若干组

SELECT 列，分组函数

FROM 表

[WHERE 筛选条件]

[GROUP BY 分组的列表]

[HAVING 筛选条件]			（分组后的筛选）

[ORDER BY 子句]

<!--group by子句支持单个字段分组，多个字段分组（多个字段之间用逗号隔开没有顺序要求）-->



### 连接查询（多表查询）

##### 一、sql92语法

##### 按功能分类：

​	1、内连接：

​		1）等值连接

​			语法：select 查询列表

​						from 表1 别名，表2 别名

​						where 连接条件

​			案例：查询女生名和对应的男生名

​			SELECT name，boyName FROM boys，beauty WHERE beauty.boyfriend_id=boys.id;

​		2）非等值连接

​			案例：查询员工的工资和工资级别

​			SELECT salary,grade_level FROM employees e,job_grades g WHERE salary BETWEEN g.'lowest_sal' AND g.'hightest_sal';

​		3）自连接

​	2、外连接：

​		1）左外连接

​		2）右外连接

​		3）全外连接(mysql不支持)

​	3、交叉连接 



##### 二、sql99语法

**语法：**select 查询列表

​			from 表1 别名 【连接类型】

​			join 表2 别名

​			on 连接条件

​			【where 筛选条件】

​			【group by 分组】

​			【having 筛选条件】

​			 【order by 排序列表】

##### 按功能分类：

​	**1、内连接：**inner

​		1）等值连接

​		2）非等值连接

​			案例：查询员工的工资和工资级别

​			SELECT salary,grade_level 

​			FROM employees e 

​			JOIN job_grades g 

​			ON e.'salary' BETWEEN g.'lowest_sal' AND g.'hightest_sal';

​		3）自连接

​	**2、外连接：**

​		应用场景：用于查询一个表中有，另一个表没有的记录

​		特点：

​		外连接的查询结果为主表中的所有记录

​			如果从表中有和它匹配的，则显示匹配的值

​			如果从表中没有和它匹配的，则显示null

​			外连接查询结果=内连接结果+主表中有而从表没有的记录

​		1）左外连接：left 【outer】左边的是主表

​		2）右外连接:  right 【outer】右边的是主表

​		3）全外连接(mysql不支持)：full 【outer】

​	**3、交叉连接 ：**cross

​	特点：
​		类似于笛卡尔乘积



### 子查询

**含义：**嵌套在其他语句中的select语句，称为子查询或内查询

​			外部的查询语句，称为主查询或外查询

**分类：**

​	按子查询出现的位置：

​			select后面：仅仅支持标量子查询

​			from后面：支持表子查询

​			**where或having后面：**标量子查询（单行），列子查询（多行），行子查询（一行多列）

​				特点：

​					1）子查询放在子括号内

​					2）子查询一般放在条件的右侧

​					3）标量子查询，一般搭配着单行操作符使用  >  <  >=  <=  =  <>

​						  列子查询，一般搭配着多行操作符使用  in、any/some、all

​			exists后面（相关子查询）：表子查询

​				语法：exists（完整的查询语句）

​				结果：1或0

按结果集的行列数不同：

​		标量子查询（结果集只有一行一列）

​		列子查询（结果集只有一列多行）

​		行子查询（结果集有一行多列）

​		表子查询（结果集一般为多行多列）

### 分页查询

应用场景：当要显示的数据，一页显示不全，需要分页提交sql请求

语法：

​			select 查询列表

​			from 表

​			【join type join 表2

​			on 连接条件

​			where 筛选条件

​			group by 分组字段

​			having 分组后的筛选

​			order by 排序的字段】

​			limit offset，size；

​			offset要显示条目的起始索引（起始索引从0开始）

​			size要显示的条目个数

特点：

​			1）limit语句放在查询语句的最后

​			2）公式

​			要显示的页数page，每页的条目数size

​			select 查询列表

​			from 表

​			limit （page-1）d*size，size；

​			size=10

​			page

​			1					0

​			2					10

​			3					20

### 联合查询

union 联合 ：将多条查询语句的结果合并成一个结果

语法：

查询语句1

union

查询语句2

union

...

应用场景：

要查询的结果来自于多个表，且多个表没有直接的连接关系，但查询的信息一致时

特点：

1、要求多条查询语句的查询列数是一致的

2、要求多条查询语句的查询的每一列的类型和顺序最好一致

3、union关键字默认去重，如果使用union all可以包含重复项



## DML语言（数据操作语言）

insert

update

delete

#### 一、插入语句

**1、方式一：**语法：insert into 表名（列名，...） values（值1，...）；

插入的值的类型要与列的类型一致或兼容

INSERT INTO beauty(id,NAME,sex,borndate,phone,photo,boyfriend_id)  VALUES(13,'唐艺兴','女','1990-4-23','18932423454',NULL,2);

可以调换顺序

可以省略列名，默认所有列，而且列的顺序和表中列的顺序一致

**2、方式二：**语法：insert into 表明 set 列名=值，列名=值，...

#### 二、修改语句

1、修改单表的记录

语法：

update 表名 set 列=新值，列=新值，... where 筛选条件；

2、修改多表的记录

语法：

update 表1 别名，表2 别名 set 列=值,... where 连接条件 and 筛选条件（sql92语法）

update 表1 别名 inner|left|right join 表2 别名 on 连接条件 set 列=值,... where 筛选条件；

#### 三、删除语句

方式一：delete

语法：

1、单表的删除

delete from 表名 where 筛选条件；

2、多表的删除

delete 别名 from 表1 别名，表2 别名 where 连接条件 and 筛选条件；（sql92语法）

delete 表1的别名，表2的别名 from 表1 别名 inner|left|right join 表2 别名 on 连接条件 where 筛选条件；（sql99语法）

方式二：truncate

语法：truncate table 表名；

## DDL语言

#### 库和表的管理

###### 创建库Books

CREATE DATABASE IF NOT EXISTS books；

###### 库的修改

RENAME DATABASE books TO 新库名；

###### 更改库的字符集

ALTER DATABASE books CHARACTER SET gbk；

###### 库的删除

DROP DATABASE IF EXISTS 库名； 

###### 表的创建

CREATE TABLE IF NOT EXISTS author（

​				id INT，

​				au_name VARCHAR（20），

​				nation VARCHAR（10）

） DESC author；

###### 表的修改

alter table 表名 add|drop|modify|change column 列名 【列类型 约束】

1）修改列名

ALTER TABLE book CHANGE COLUMN publishdate pubDate DATETIME；

2）修改列的类型或约束

ALTER TABLE book MODIFY COLUMN pubdate TIMESTAMP；

3）添加新列

ALTER TABLE author ADD COLUMN annual DOUBLE；

4）删除列

ALTER TABLE author DROP COLUMN annual；

5）修改表名

ALTER TABLE author RENAME TO book_author;

###### 表的删除

DROP TABLE book_author;

SHOW TABLES;

###### 表的赋值

1、仅仅复制表的结构

CREATE TABLE copy LIKE author；

#### 常见数据类型的介绍

**一、整型**

特点：1）如果不设置无符号还是有符号，默认是有符号，如果想设置无符号，需要添加unsigned关键字

2）如果插入的数值超出了整型的范围，会报out of range异常，并且插入临界值

3）如果不设置长度，会有默认的长度

长度代表了显示的最大宽度，如果不够会用0在左边填充，但必须搭配zerofill使用

**二、小数**

1、浮点型

float（M，D）

double（M，D）

2、定点型

dec（M，D）

decimal（M，D）

特点：1）M：整数部位+小数部位

D：小数部位（如果超过范围，则插入临界值）

2）M和D都可以省略

如果是decimal，则M默认为10，D默认为0

如果是float和double，则会根据插入的数值的精度来决定精度

3）定点型的精确度较高，如果要求插入数值的精度较高如货币运算等则考虑使用

**三、字符型**

较短的文本：char（固定长度的字符）和varchar（可变长度的字符）

较长的文本：text和blob

**四、日期型**

datetime

timestamp

#### 常见约束

**含义：**一种限制，用于限制表中的数据，为了保证表中的数据的准确和可靠性

**分类：**六大约束

​			NOT NULL：非空，用于保证该字段的值不能为空

​			DEFAULT：默认，用于保证该字段有默认值

​			PRIMARY KEY：主键，用于保证该字段的值具有唯一性

​			UNIQUE：唯一，用于保证该字段的值具有唯一性，可以为空

​			CHECK：检查约束【mysql中不支持】

​			FOREIGN KEY：外键，用于限制两个表的关系，用于保证该字段的值必须来自于主表的关联列的值

**添加约束的实际：**

​				创建表时

​				修改表时

**约束的添加分类：**

​				列级约束：

​							六大约束语法上都支持，但外键约束没有效果,一般将外键约束部分写为表级约束

​				表级约束：

​							除了非空、默认、其他的都支持

外键：

​		1、要求在从表设置外键关系

​		2、从表的外键列的类型和主表的关联列的类型要求一致或兼容，名称无要求

​		3、主表的关联列必须是一个key（一般是主键或唯一）

​		4、插入数据时，先插入主表，再插入从表

​		删除数据时，先删除从表，再删除主表

**一、创建表时添加约束**

1、添加列级约束

USE  students；

CREATE TABLE stuinfo{

​			id INT PRIMARY KEY，		主键

​			stuName VARCHAR（20） NOT NULL，		非空

​			gender CHAR(1) CHECK(gender='男' OR gender='女'),			检查

​			seat INT UNIQUE，				唯一

​			age INT DEFAULT 18，			默认约束

​			majorId INT FOREIGN KEY REFERENCES major(id)				外键

）

CREATE TABLE major（

​			id INT PRIMARY KEY，

​			majorName VARCHAR(20)

)

2、添加表级约束

语法：在各个字段的最下面

【constraint 约束名】 约束类型(字段名)

DROP TABLE IF EXISTS stuinfo;

CREATE TABLE stuinfo(

​			id INT,

​			stuname VARCHAR(20),

​			gender CHAR(1),

​			seat INT,

​			age INT,

​			majorid INT,

​			CONSTRAINT pk PRIMARY KEY(id),	主键

​			CONSTRAINT uq UNIQUE(seat),	唯一键

​			CONSTRAINT ck CHECK(gender='男' OR gender='女'),	检查

​			CONSTRAINT fk_stuinfo_major FOREIGN KEY(majorid) REFERENCES major(id)	外键

)；

SHOW INDEX FROM stuinfo;

**二、修改表时添加约束**

列级约束语法：

ALTER TABLE 表名 MODIFY COLUMN 列名 列类型 约束类型

表级约束语法：

alter table 表名 add 【constraint 约束名】 约束类型（字段名） 【外键的引用】；

1、添加主键

1）列级约束

ALTER TABLE stuinfo MODIFY COLUMN id INT PRIMARY KEY；

2）表级约束

ALTER TABLE stuinfo ADD PRIMARY KEY(id);

2、添加外键

ALTER TABLE stuinfo ADD 【CONSTRAINT fk_stuinfo_major】 FOREIGN KEY(majorid) REFERENCES major(id);

**三、修改表时删除约束**

1、删除非空约束

ALTER TABLE stuinfo MODIFY COLUMN stuname VARCHAR(20) NULL;

2、删除默认约束

ALTER TABLE stuinfo MODIFY COLUMN age INT；

3、删除主键

ALTER TABLE stuinfo DROP PRIMARY KEY；

4、删除唯一

ALTER TABLE stuinfo DROP INDEX seat；

SHOW INDEX FROM stuinfo;



### 标识列（自增长列）

**含义：**可以不用手动插入值，系统提供默认的序列值

**特点：**

1、

一、创建表时设置标识列

CREATE TABLE tab_identity(

​				id INT PRIMARY KEY AUTO_INCREMENT,

​				NAME VARCHAR(20)

);

INSERT INTO tab_identity VALUES(NULL,'john);

二、修改表时设置标识列

ALTER TABLE tab_identity MODIFY COLUMN id INT PRIMARY KEY AUTO_INCREMENT；

三、修改表时删除标识列

ALTER TABLE tab_identity MODIFY COLUMN id INT； 



## TCL（事务控制语言）

**事务：**

一个或一组sql语句组成一个执行单元，这个执行单元要么全部执行，要么全部不执行。

在mysql中用的最多的存储引擎有：innodb，myisam，memory等。其中innodb支持事务，其余不支持。

**事务的ACID属性：**

1、原子性：原子性是指事务是一个不可分割的工作单位，事务中的操作要么都发生，要么都不发生。

2、一致性：事务必须使数据库从一个一致性状态变换到另一个一致性状态

3、隔离性：事务的隔离性是指一个事务的执行不能被其他事务干扰，即一个事务内部的操作及使用的数据对并发的其他事务是隔离的，并发执行的各个事务之间不能互相干扰。

4、持久性：一个事务一旦提交，则会永久的改变数据库的数据。

**事务的创建：**

隐式事务，事务没有明显的开启和结束的标记

比如insert、update、delete语句

delete from 表 where id=1；

显式事务：事务具有明显的开启和结束的标记

前提：必须先设置自动提交功能为禁用（set autocommit=0；）

set autocommit=0；

start transaction；		可选的

语句1；

语句2；

...

commit；			提交事务

rollback；			回滚事务





#### 数据库的隔离级别

对于同时运行的多个事务，当这些事务访问数据库中相同的数据时，如果没有采取必要的隔离机制，就会导致各种**并发问题：**

​	**脏读：**对于两个事务T1，T2，T1读取了已经被T2更新但还没有被提交的字段。之后，若T2回滚，T1读取的内容就是临时且无效的

​	**不可重复读：**对于两个事务T1，T2，T1读取了一个字段，然后T2更新了该字段，之后，T1再次读取同一个字段，值就不同了。

​	**幻读：**对于两个事务T1，T2，T1从一个表中读取了一个字段，然后T2在该表中插入了一些新的行，之后如果T1再次读取同一个表，就会多出几行。

##### 数据库提供的4种事务隔离级别：

**READ UNCOMMITTED(读未提交数据)**：允许事务读取未被其他事务提交的变更，脏读、不可重复读和幻读的问题**都会出现**。

**READ COMMITED(读已提交数据)**：只允许事务读取已经被其他事务提交的变更，**可避免脏读**，但不可重复读和幻读问题仍然可能出现

**REPEATABLE READ(可重复读)**：确保事务可以多次从一个字段中读取相同的值，在这个事务持续期间，禁止其他事务对这个字段进行更新，**可以避免脏读和不可重复读**，但幻读仍存在

**SERIALIZABLE(串行化)**：确保事务可以从一个表中读取相同的行，在这个事务持续期间，禁止其他事务对该表执行插入，更新和删除操作，**可避免所有并发问题**，但性能十分低下

Oracle支持2种事务隔离级别：READ COMMITED（默认），SERIALIZABLE。

Mysql支持4种事务隔离级别：默认为REPEATABLE READ

+ 每启动一个mysql程序，就会获得一个单独的数据库连接，每个数据库连接都有一个全局变量@@tx_isolation,表示当前的事务隔离级别。
+ 查看当前的隔离级别：SELECT @@tx_isolation;
+ 设置当前mysql连接的隔离级别：set transaction isolation level read committed；
+ 设置数据库系统的全局的隔离级别：set global transaction isolation level read committed；

保存点		savepoint

### 视图

含义：虚拟表，和普通表一样使用

1、创建视图：

CREATE VIEW v1 AS 

SELECT stuname，majorname FROM stuinfo s INNER JOIN major m ON s.'majorid'=m.'id'；

SELECT * FROM v1 WHERE stuname LIKE '张%'；

2、修改视图：

方式1：create or replace view 视图名 as 查询语句；

方式2：alter view 视图名 as 查询语句；

3、删除视图：

drop view 视图名，视图名，...；

**具备以下特点的视图不允许更新：**

1）包含以下关键字的sql语句：分组函数，distinct，group by，having，union或者union all

2）常量视图

CREATE OR REPLACE VIEW myv2 AS SELECT ‘john’ NAME；

3）select中包含子查询

4）含有join语句能update但不能insert

5）from一个不能更新的视图

6）where子句的子查询引用了from子句中的表



delete（支持回滚）和truncate（不支持回滚）在事务使用时的区别

### 变量

###### 系统变量

​			**全局变量**：针对所有的会话连接有效

​			**会话变量**：针对于当前的会话（连接）有效

###### 自定义变量

​			**用户变量**：针对于当前会话（连接）有效（可以放在任何地方）

​			**局部变量**：仅仅在定义它的begin end中有效，只能在begin end中的第一句话声明（只可以放在begin end中）

###### 一、系统变量

由系统提供，不是用户定义，属于服务器层面

语法：1、查看所有系统变量

show global|【session】 variables；

2、查看满足条件的部分系统变量

show global|【session】 variables like ‘%char%’；

3、查看指定的某个系统变量的值

select @@global|【session】系统变量名；

4、为某个系统变量赋值

方式1：set global|【session】 系统变量名 = 值；

方式2：set @@global|【session】.系统变量名 = 值；

###### 二、自定义变量

用户自定义的，不是由系统提供

**1、用户变量**

1）声明并初始化

SET @用户变量名=值；	或		

SET @用户变量名：=值；		或		

SELECT @用户变量名：=值；

2）赋值（更新用户变量的值）

SET @用户变量名=值；		或

SET @用户变量名:=值；		或

SELECT @用户变量名：=值；		或

SELECT 字段 INTO @变量名 FROM 表；

3）使用（查看用户变量的值）

SELECT @用户变量名；

**2、局部变量**

1）声明

DECLARE 变量名 类型；

DECLARE 变量名 类型 DEFAULT 值；

2）赋值

SET 局部变量名=值；		或

SET 局部变量名:=值；		或

SELECT @局部变量名：=值；		或

SELECT 字段 INTO 局部变量名 FROM 表；

3）使用

SELECT 局部变量名；

### 存储过程和函数

##### 存储过程

**语法：**

1、创建：CREATE PROCEDURE 存储过程名（参数列表）

BEGIN

​			存储过程体（一组合法的SQL语句）

END								结束标记可以通过DELIMITER重新设置

参数列表包含三部分：参数模式	参数名	参数类型

参数模式：IN，OUT，INOUT

2、调用

CALL 存储过程名（实参列表）；

3、删除

DROP PROCEDURE 存储过程名；

4、查看存储过程的信息

SHOW CREATE PROCEDURE 存储过程名；

##### 函数

**与存储过程的区别：**
存储过程：可以有0个返回，也可以有多个返回，适合做批量插入、批量更新

函数：有且仅有1个返回，适合做处理数据后返回一个结果

**语法：**

1、创建

CREATE FUNCTION 函数名（参数列表） RETURNS 返回类型

BEGIN

​			函数体

END									结束标记可以通过DELIMITER重新设置（结束标记一般设置为$)

参数列表包含两部分：

参数名	参数类型

2、调用

SELECT 函数名（参数列表） $								结束标记为$的情况

3、查看

SHOW CREATE FUNCTION 函数名；

4、删除

DROP FUNCTION 函数名；

### 流程控制结构

顺序结构：程序从上往下依次执行

分支结构：程序从两条或多条路径中选择一条去执行

循环结构：程序在满足一定条件的基础上，重复执行一段代码

一、分支函数

1、if函数

语法：

SELECT IF（表达式1，表达式2，表达式3）

如果表达式1成立，则返回表达式2的值，否则返回表达式3的值

2、case结构

语法：

（1）switch case的效果

case 要判断的字段或表达式

when 常量1 then 要显示的值1或语句1

when 常量2 then 要显示的值2或语句2

...

else 要显示的值n或语句n

end

（2）多重if的效果

case

when 条件1 then 要显示的值1或语句1

when 条件2 then 要显示的值2或语句2

...

else 要显示的值n或语句n

end

特点：

可以作为表达式，嵌套在其他语句中使用，可以放在任何地方，BEGIN END中或BEGIN END的外面。

可以作为独立的语句去使用，只能放在BEGIN END中。

3、if结构（实现多重分支）

语法：

if 条件1 then 语句1；

elseif 条件2 then 语句2；

...

【else 语句n；】

end if；

二、循环结构（只能放在begin end中）

分类：

while、loop、repeat

循环控制：

iterate类似于continue，结束本次循环，继续下一次

leave类似于break，结束当前所在循环

1、while（先判断后执行）

语法：

【标签：】while 循环条件 do

​			循环体；

end while 【标签】；

2、loop（没有条件的死循环）

语法：

【标签：】loop

​			循环体；

end loop 【标签】；

可以用来模拟简单的死循环

3、repeat（先执行后判断）

语法：

【标签：】 repeat

​			循环体；

until 结束循环的条件

end repeat 【标签】；

## 数据模型

数据模型（Data Model）是数据特征的抽象，它从抽象层次上描述了系统的静态特征、动态行为和约束条件。数据模型所描述的内容有三部分：数据结构、数据操作和数据约束。

### 数据模型的分类

数据模型按不同的应用层次分为三种类型：概念数据模型、逻辑数据模型、物理数据模型。数据发展过程中产生过三种基本的数据模型：层次模型、网状模型和关系模型。

#### 层次模型

层次模型是数据库系统最早使用的一种模型。它的数据结构是一棵“有向树”。根结点在最上端，层次最高，子结点在下，逐层排列。特征是：

* 有且只有一个根结点
* 其他结点有且只有一个父结点

优点：

* 简单清晰
* 查询效率高
* 提供了良好的完整性支持

缺点：

* 多对多联系表示不自然
* 查询子女节点必须通过双亲节点
* 对插入和删除操作的限制多，程序编写较复杂

#### 网状模型

网状模型以网状结构表示实体与实体之间的联系。网中的每一个结点代表一个记录类型，联系用链接指针来实现。网状模型可以表示多个从属关系的联系，也可以表示数据间的交叉关系，即数据间的横向关系与纵向关系，它是层次模型的扩展。特征是：

* 允许结点有多于一个父结点
* 可以有一个以上的结点没有父结点

优点：

* 可方便的表示现实世界中很多复杂的关系

* 修改时没有层次模型那么多严格的限制
* 实体间的关系在底层中可以借由指针实现，效率较高

缺点：

* 结构复杂，不利于数据库的维护和重建
* 彼此关联大，需说明的东西较多
* DDL、DML语言复杂，不易使用
* 记录之间联系是通过存取路径实现的，用户必须了解系统结构的细节

#### 关系模型（目前最流行）

关系模型以**二维表**结构来表示实体与实体之间的联系。每个二维表又可称为关系。在关系模型中，操作的对象和结构都是二维表。支持关系模型的数据库管理系统称为**关系数据库管理系统**。特征是：

* 描述的一致性，不仅用关系描述实体本身，而且也用关系描述实体之间的联系
* 可直接表示多对多的联系
* 关系必须是规范化的关系，即每个属性是不可分的数据项，不许表中有表
* 关系模型是建立在数学概念基础上的，有较强的理论依据

优点：

* 结构简单
* 存取路径对用户而言完全隐蔽，程序和数据具有高度的独立性
* 操作方便

缺点：

* 查询效率低，关系数据模型提供了较高的数据独立性和非过程化的查询功能，因此加大了系统的负担
* 由于查询效率较低，因此需要数据库系统对查询进行优化，加大了DBMS的负担

##### 相关概念：

关系（Relation）：一个关系对应通常说的一张表。

元组（Tuple）：表中的一行即为一个元组。

属性（Attribute）：表中的一列即为一个属性，给每一个属性起一个名称即属性名。

主码（Key）：也称码键。表中的某个属性组，可以唯一确定一个元组。

域（Domain）：是一组具有相同数据类型的值得集合，属性的取值范围来自某个域。

分量：元组中的一个属性值。

关系模式：对关系的描述，关系名（属性1，属性2，...，属性n）。

##### 关系的完整性约束条件

1. 实体完整性

   实体完整性是指实体的主属性不能取空值。实体完整性规则规定实体的所有主属性都不能为空。实体完整性针对基本关系而言的，一个基本关系对应着现实世界中的一个主题。

2. 参照完整性

   在关系数据库中主要是值得外键参照的完整性。若A关系中的某个或者某些属性参照B或其他几个关系中的属性，那么在关系A中该属性要么为空，要么必须出现B或者其他的关系的对应属性中。

3. 用户定义的完整性

   用户定义完整性是针对某一个具体关系的约束条件。它反映的某一个具体应用所对应的数据必须满足一定的约束条件。

###### 起别名

1、AS

2、空格

###### 去重

distinct

###### 模糊查询

like，一般与通配符搭配使用，%零个或多个字符，_单个字符

###### 查询排序

DESC降序，ASC升序

order by一般在查询语句的最后，除了limit

###### 安全等于			<=>

###### 不等于				<>