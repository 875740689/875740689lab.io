﻿---
title: Spring MVC回顾
//toc: true
//thumbnail: 
date: 2020-04-07 11:46:40
tags:
 - Spring MVC
categories:
 - 后端开发
---

Spring框架提供了构建Web应用程序的全功能MVC模块--Spring MVC。Spring MVC框架提供了一个DispatcherServlet作用前端控制器来分派请求，同时提供灵活的配置处理程序映射、视图解析、语言环境和主题解析，并支持文件上传。Spring MVC还包含多种视图技术。Spring MVC分离了控制器、模型对象、分派器以及处理程序对象的角色，这种分离让它们更容易进行定制。<!--more-->

## Spring MVC的特点

+ Spring MVC拥有强大的灵活性、非侵入性和可配置性。
+ Spring MVC提供了一个前端控制器DispatcherServlet，开发者无须额外开发控制器对象。
+ Spring MVC分工明确，包括控制器、验证器、命令对象、模型对象、处理程序映射视图解析器等等，每一个功能实现由一个专门的对象负责完成。
+ Spring MVC可以自动绑定用户输入，并正确地转换数据类型。例如，Spring MVC能自动解析字符串，并将其设置为模型的int或float类型的属性。
+ Spring MVC使用一个名称/值的Map对象实现更加灵活的模型数据传输。
+ Spring MVC内置了常见的校验器，可以校验用户输入，如果校验不通过，则重定向回输入表单。输入校验是可选的，并且支持编程方式及声明方式。
+ Spring MVC支持国际化，支持根据用户区域显示多国语言。

## 一个简单的Spring MVC应用

### 1、引入maven依赖
```xml
<dependencies>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-webmvc</artifactId>
            <version>4.2.0.RELEASE</version>
        </dependency>
</dependencies>
```
### 2、配置DispatcherServlet
许多的MVC框架中，都包含一个用于调度控制的Servlet。Spring MVC也提供了一个前端控制器DispatcherServlet，所有的请求驱动都围绕这个DispatcherServlet来分派请求。
DispatcherServlet是一个Servlet(它继承自HttpServlet基类)，因此使用时需要把它配置在Web应用的部署描述符web.xml文件当中，配置信息如下：
```xml
<servlet>
	<!--Servlet的名称-->
	<servlet-name>springmvc</servlet-name>
	<!--Servlet对应的java类-->
	<servlet-class>org.springframework.web.servlet.DispatcherServlet</servlet-class>
	<!--当前Servlet的参数信息-->
	<init-param>
		<!--contextConfigLocation是参数名称，该参数的值包含Spring MVC的配置文件路径-->
		<param-name>contextConfigLocation</param-name>
		<param-value>/WEB-INF/springmvc-config.xml</param-value>
	</init-param>
	<!--在Web应用启动时立即加载Servlet-->
	<load-on-startup>1</load-on-startup>
</servlet>
<!--Servlet映射声明-->
<servlet-mapping>
	<!--请求对应的Servlet的名称-->
	<servlet-name>springmvc</servlet-name>
	<!--监听当前域的所有请求-->
	<url-pattern>/</url-pattern>
</servlet-mapping>
```
配置了一个DispatcherServlet，该Servlet在Web应用程序启动时立即加载，DispatcherServlet加载时会需要一个Spring MVC的配置文件，默认情况下，应用会去应用程序文件夹的WEB-INF下查找对应的[servlet-name]-servlet.xml文件，例如本例的<servlet-name>是springmvc，默认查找的就是/WEB-INF/springmvc-servlet.xml。
本例中通过init-param元素的描述，将查找的Spring MVC配置文件位置修改为/WEB-INF/springmvc-config.xml，解析该文件内容并根据文件配置信息创建一个WebApplicationContext容器对象，也称为上下文环境。
WebApplication继承自ApplicationContext容器，它的初始化方式和BeanFactory、ApplicationContext有所区别，因为WebApplicationContext需要ServletContext实例，它必须在拥有Web容器的前提下才能完成启动Spring Web应用上下文的工作。

### 3、Controller类的实现
```java
package com.jenson.controller;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class HelloController{
	private static final Log logger = LogFactory.getLog(HelloController.class);

	@RequestMapping(value="/hello")
	public ModelAndView hello(){
		logger.info("hello方法被调用");
		//创建准备返回的ModelAndView对象，该对象通常包含了返回视图名、模型的名称以及模型对象
		ModelAndView mv = new ModelAndView();
		//添加模型数据，可以是任意的POJO对象
		mv.setViewName("/WEB-INF/content/welcome.jsp");
		//返回ModelAndView对象
		return mv;
	}
}
```
### 4、配置Spring MVC的Controller
本例的配置文件位置在/WEB-INF/springmvc-config.xml。
```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://www.springframework.org/schema/beans
		http://www.springframework.org/schema/beans/spring-beans-4.2.xsd">
	<!--配置Handle，映射“/hello”请求-->
	<bean name="/hello" class="com.jenson.controller.HelloController"/>
	<!--处理映射器将bean的name作为url进行查找，需要在配置Handle时指定name(即url)-->
	<bean class="org.springframework.web.servlet.handler.BeanNameUrlHandlerMapping"/>
	<!--SimpleControllerHandlerAdapter是一个处理器适配器，所有处理适配器都要实现HandlerAdapter接口-->
	<bean class="org.springframework.web.servlet.mvc.SimpleControllerHandlerAdapter"/>
	<!--视图解析器-->
	<bean class="org.springframework.web.servlet.view.InternalResourceViewResolver"/>
</beans>
```
### 5、View页面
该应用包含一个视图页面welcome.jsp，用来显示欢迎信息。
```html
<%@page language="java" contentType="text/html;charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>welcome</title>
</head>
<body>
<!-- 页面可以访问Controller传递出来的message -->
${requestScope.message}
</body>
</html>
```
## 详解DispatcherServlet
在一个Spring MVC应用运行过程中，前端控制器DispatcherServlet具体的作用是什么呢？
分析DispatcherServlet源代码如下：
```java
protected void initStrategies(ApplicationContext context){
	initMultipartResolver(context);		//初始化上传文件解析器
	initLocaleResolver(context);		//初始化本地化解析器
	initThemeResolver(context);		//初始化主题解析器
	initHandlerMappings(context);		//初始化处理器映射器，将请求映射到处理器
	initHandlerAdapters(context);		//初始化处理器适配器
	initHandlerExceptionResolvers(context);		//初始化处理器异常解析器，如果执行过程中遇到异常将交给HandlerExceptionResolver来解析
	initRequestToViewNameTranslator(context);	//初始化请求到视图名称解析器
	initViewResolvers(context);		//初始化视图解析器，通过ViewResolver解析逻辑视图名到具体视图实现
	initFlashMapManager(context);		//初始化flash映射管理器
}
```
initStrategies方法将在WebApplicationContext初始化后自动执行，自动扫描上下文的Bean，根据名称或类型匹配的机制查找自定义组件，如果没有找到则会装配一套Spring的默认组件。
