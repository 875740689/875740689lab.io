---
title: Webpack
date: 2020-04-29 22:29:34
tags:
 - Webpack
---

Webpack是一个现代JavaScript应用程序的静态模块打包器，当Webpack处理应用程序时，它会递归地构建一个依赖关系图，其中包含应用程序需要的每个模块，然后将所有这些模块打包成一个或多个bundle。<!--more-->



## 为什么要使用Webpack



​	以前的前端，很多静态资源、CSS、图片和JS都是手动引入HTML页面中，杂乱无章的代码混在一个文件中，想要寻找某个功能的代码很是困难，要是分开多个文件引入，又会造成HTTP请求数过多的问题。

​	为了解决这个问题，出现了许多模块化工具，其中，Webpack具有模块化和组件化的特性，它将一个项目看做一个整体，简化了开发的复杂度，提高了我们的开发效率。

![1031000-160bc667d3b6093a](D:\Java\blog\source\_posts\webpack\1031000-160bc667d3b6093a.png)

除此之外，Webpack在资源处理方面具有一定优势，它通过代码拆分来做资源异步加载，会消除对未引用资源的依赖，能够控制资源的处理方式，从而加快处理速度。



## Webpack的使用

初步了解了Webpack的优势后，接着了解如何使用Webpack。

### 安装

Webpack可以使用npm安装，新建一个项目，在终端中转到该项目所在位置后执行下述指令就可以完成安装。

```javascript
//全局安装
npm install -g webpack
//安装到项目目录
npm install --save--dev webpack
```



### 使用Webpack的准备工作

1.在项目中创建一个package.json文件，这是一个标准的npm说明文件