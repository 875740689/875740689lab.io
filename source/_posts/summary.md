---
title: Java基础知识点
date: 2020-04-07 17:43:40
thumbnail: 
//toc: true
tags:
 - Java基础
 - 总结
categories:
 - Java
---



#### **Java基础**



##### **1、Java面向对象编程的三大特性：**

封装、继承、多态
注意：继承时父类中的私有属性和方法，子类无法访问，只是拥有。
<!--more-->



##### **2、String、StringBuffer和StringBuilder的区别是什么？**

+ 可变性
String类中使用final关键字修饰字符数组来保存字符串，所以String对象是不可变的。
StringBuffer和StringBuilder都继承自AbstactStringBuilder类，也是使用字符数组保存字符串，但是没有用final关键字修饰，所以这两种对象是可变的。
+ 线程安全性
String中的对象是不可变的，可以理解为常量，线程安全。StringBuffer对方法加了同步锁或者对调用的方法加了同步锁，所以是线程安全的。StringBuilder并没有对方法进行加同步锁，所以是非线程安全的。
+ 性能
每次对String类型进行改变时，都会生成一个新的String对象，然后将指针指向新的String对象。StringBuffer每次都会对StringBuffer对象本身进行操作，而不是生成新的对象并改变对象引用。相同情况下使用StringBuilder相比使用StringBuffer仅能获得10%~15%左右的性能提升，但要冒多线程不安全的风险。



##### **3、在一个静态方法内调用一个非静态成员为什么是非法的？**

由于静态方法可以不通过对象进行调用，因此在静态方法里，不能调用其他非静态变量，也不可以访问非静态变量成员。



##### **4、接口与抽象类的区别？**

接口的方法默认是**public**（Java9开始允许定义私有方法），所有方法在接口中不能有实现（Java8开始接口方法可以有默认实现），而抽象类可以有非抽象的方法。
接口中除了**static**、**final**变量，不能有其他变量，而抽象类中则不一定。
一个类可以实现多个接口，但只能实现一个抽象类。接口自己本身可以通过extends关键字扩展多个接口。



##### **5、==与equals**

==：基本数据类型比较的是值，引用数据类型比较的是内存地址。
equals()：(1)类没有覆盖equals()方法。则比较的是内存地址。(2)类覆盖了equals()方法，比较的是内容是否相等，
注意：String中的equals方法是被重写过的，比较的是对象的值。当创建String类型的对象时，虚拟机会在常量池中查找有没有已经存在的值和要创建的值相同的对象，如果有就把它赋给当前引用。如果没有就在常量池中重新创建一个String对象。



**6、为什么要有hashCode？**

当要将对象加入HashSet时，HashSet会先计算对象的hashcode值来判断对象加入的位置，同时也会与该位置其他已经加入的对象的hashcode值作比较，如果相等，则调用equals()方法来检查hashcode相等的对象是否真的相同。如果两者相同，HashSet就不会让其加入成功。前面的hashcode判断大大地减少了equals的次数，相应就大大 提高了执行速度。

> 如果两个对象相等，则hashcode一定也是相同的。
>
> 两个对象有相同的hashcode值，它们也不一定时相等的。
>
> equals方法被覆盖过，则hashCode方法也必须被覆盖。



##### **7、既然有了字节流，为什么还要有字符流？**

不管是文件读写还是网络发送接收，信息的最小存储单元都是字节，那么为什么I/O流操作要分为字节流操作和字符流操作呢？
字符流是由Java虚拟机将字节转换得到的，问题就出在这个过程还算是非常耗时，并且，如果我们不知道编码类型就很容易出现乱码问题。所以，I/O流就干脆提供了一个直接操作字符的接口，方便我们平时对字符进行流操作。



##### **8、BIO，NIO，AIO有什么区别？**

+ BIO：同步阻塞I/O模式，数据的读取写入必须阻塞在一个线程内等待其完成。在活动连接数不是特别高（小于单机1000）的情况下，这种模型是比较不错的，可以让每一个连接专注于自己的I/O并且编程模型简单，也不用过多考虑系统的过载、限流等问题。线程池本身就是一个天然的漏斗，可以缓冲一些系统处理不了的连接或请求。但是，在面对十万甚至百万级连接时，传统的BIO模型是无能为力的。
+ NIO：NIO是一种同步非阻塞的I/O模型，在Java1.4中引入NIO框架，对应java.nio包，提供了Channel，Selector，Buffer等抽象。NIO中的N可以理解为Non-blocking，不单纯是New。它支持面向缓冲的，基于通道的I/O操作方法。NIO提供了与传统BIO模型中的Socket和ServerSocket相对应的SocketChannel和ServerSocketChannel两种不同的套接字通道实现，两种通道都支持阻塞和非阻塞两种模式，阻塞模式使用就像传统中的支持一样，比较简单，但是性能和可靠性都不好；非阻塞模式正好与之相反。对于低负载、低并发的应用程序，可以使用同步阻塞I/O来提升开发速率和更好的维护性，对于高负载、高并发的(网络)应用，应使用NIO的非阻塞模式来开发
+ AIO：AIO也就是NIO2。在Java7中引入了NIO的改进版NIO2，它是异步非阻塞的IO模型。异步IO是基于事件和回调机制实现的，也就是应用操作之后会直接返回，不会阻塞在哪里，当后台处理完成，操作系统会通知相应的线程进行后续的操作。AIO是异步IO的错写，虽然NIO在网络操作中，提供了非阻塞的方法，但NIO的IO行为还是同步的。对于NIO来说，我们的业务线程是在IO操作准备好是，得到通知，接着就由这个线程自行进行IO操作，IO操作本身是同步的。目前AIO的应用还不是很广泛。



##### **9、四种权限修饰符**

|              | public | protected | (default) | private |
| ------------ | ------ | --------- | --------- | ------- |
| 同一个类     | YES    | YES       | YES       | YES     |
| 同一个包     | YES    | YES       | YES       | NO      |
| 不同包子类   | YES    | YES       | NO        | NO      |
| 不同包非子类 | YES    | NO        | NO        | NO      |

不想外部能访问，而子类能访问的变量就定义成保护变量。**protected和private不能修饰类（指外部类）**，只能修饰数据成员，构造方法，方法成员。



##### **10、Java变量的作用域**

成员变量：全局变量（实例变量）、静态变量（类变量）

局部变量：方法局部变量（方法内定义）、方法参数变量（形参）、代码块局部变量（代码块内定义，常用于try catch代码块，成为异常处理参数变量）。

方法中如果出现了和全局变量重名的变量：就近原则：谁离访问代码近，优先访问谁。



#### **Java集合**



##### **1、List,Set,Map的区别？**
List：不唯一，有序
Set：唯一，无序
Map：使用键值对存储，Map会维护与Key有关联的值。两个Key可以引用相同的对象，但Key不能重复，典型的Key是String类型，但也可以任何对象。



##### **2、ArrayList与LinkedList的区别？**

线程安全：ArrayList和LinkedList都是不同步的，也就是不保证线程安全；
底层数据结构：ArrayList底层使用的是Object数组，LinkedList底层使用的是双向链表；
增删数据：LinkedList插入与删除比ArrayList方便；
数据快速随机访问：ArrayList支持快速随机访问，LinkedList不支持；（RandomAccess接口作为一个标识，标识实现这个接口的类具有随机访问功能）
内存控件占用：ArrayList的空间浪费主要体现在List列表的结尾会预留一定的容量空间，而LinkedList的空间花费则体现在它的每一个元素都需要消耗比ArrayList更多的空间。



##### **3、ArrayList和Vector的区别？**
Vector类的所有方法都是同步的，可以有两个线程安全地访问一个Vector对象，但是一个线程访问Vector的话，代码要在同步操作上耗费大量的时间；ArrayList不是同步的，所以不需要保证线程安全时建议使用ArrayList。



##### **4、HashMap和HashTable的区别？**
+ 线程安全：HashMap是非线程安全的，如果需要满足线程安全，可以用Collections的synchronizedMap方法是HashMap具有线程安全的能力，或者使用ConcurrentHashMap；HashTable是线程安全的，HashTable内部的方法基本都经过synchronized修饰。
+ 效率：因为线程安全的问题，HashMap要比HashTable效率高一点。另外，HashTable基本被淘汰，不要再代码中使用它。
+ 对Null key和Null value的支持：HashMap中，null可以作为键，这样的键只有一个，可以有一个或多个键所对应的值为null。但是在HashTable中put进的键值只要有一个null，直接抛出NullPointerException。
+ 初始容量大小和每次扩充容量大小的不同：创建时如果不指定容量初始值，HashTable默认的初始大小为11，之后每次扩充，容量变为原来的2n+1。HashMap默认的初始化大小为16，之后每次扩充，容量变为原来的2倍。创建时如果给定了容量初始值，那么HashTable会直接使用你给定的大小，而HashMap会将其扩充为2的幂次方大小（HashMap中的tableSizeFor（）方法保证，下面给出了源代码）。也就是说HashMap总是使用2的幂作为哈希表的大小，后面会介绍到为什么是2的幂次方。
+ 底层数据结构：JDK1.8以后的HashMap在解决哈希表冲突时有了较大的变化，当链表长度大于阈值（默认为8）时，将链表转化为红黑树，以减少搜索时间。HashTable没有这样的机制。



##### **5、HashMap和HashSet的区别？**
HashMap：实现了Map接口，存储键值对，调用put()向map中添加元素，HashMap使用键（Key）计算Hashcode;
HashSet:实现Set接口，仅存储对象，调用add()方法向Set中添加元素，HashSet使用成员对象来计算hashcode值，对于两个对象来说hashcode可能相同，所以equals()方法用来判断对象的相等性。



##### **6、HashMap的底层实现**
JDK1.8之前HashMap底层是数组和链表结合在一起使用也就是链表散列。HashMap通过key的HashCode经过扰动函数处理过后得到hash值，然后通过（n-1)&hash判断当前元素存放的位置（这里的n指的是数组的长度），如果当前位置存在元素的话，就判断该元素与要存入的元素的hash值以及key是否相同，如果相同的话，直接覆盖，不相同就通过拉链法解决冲入。
所谓扰动函数指的就是HashMap的hash方法。使用hash方法也就是扰动函数是为了防止一些实现比较差的hashCode（）方法换句话说使用扰动函数之后可以减少碰撞。
所谓“拉链法”就是：将链表和数组相结合，也就是说创建一个链表数组，数组中每一个就是一个链表，若遇到哈希冲突，则将冲突的指加到链表中即可。（解决哈希冲突）
TreeMap、TreeSet以及JDK1.8之后的HashMap底层都用到了红黑树，红黑数就是为了解决二叉查找数的缺陷，因为二叉查找树在某些情况下会退化成一个线性结构。



##### **7、comparable和Comparator的区别？**

comparable接口实际上是出自java.lang包，它有一个compareTo(Object obj)方法用来排序
comparator接口实际上是出自java.util包，它有一个compare(Object obj1,Object obj2)方法用来排序
一般我们需要对一个集合使用自定义排序时，我们就要重写compareTo()方法或compare()方法，当我们需要对某一个集合实现两种排序方式，我们可以重写compareTo()方法和使用自制的Comparator方法或者以两个Comparator来分别实现两种排序



##### **8、为什么要有hashCode（散列码）?**

我们先以“HashSet如何检查重复”为例子来说明为什么要有hashCode：当你把对象加入HashSet时，HashSet会先计算对象的hashcode值来判断对象加入的位置，同时也会与该位置其他已经加入的对象的hashcode值作比较，如果没有相符的hashcode，HashSet会假设对象没有重复出现。但是如果发现有相同hashcode值的对象，这时会调用equals()方法来检查hashcode相等的对象是否真的相同。如果两者相同HashSet就不会让其加入操作成功，如果不同的话，就会重新散列到其他位置，这样我们就大大减少了equals的次数，相应就大大提高了执行速度。



##### **9、为什么Java中只有值传递？**

按值调用表示方法接受的是调用者提供的值，而按引用调用表示方法接收的是调用者提供的变量地址。一个方法可以修改传递引用所对应的变量值，而不能修改传递值调用所对应的变量值。Java程序设计语言总是采用按值调用，也就是说，方法得到的是所有参数值的一个拷贝，也就是说，方法不能修改传递给它的任何参数变量的内容。



##### **10、如何选用集合？**

需要根据键值获取到元素值时就选用Map接口下的集合，需要排序时选择TreeMap，不需要排序时就选择HashMap，需要保证线程安全就选用ConcurrentHashMap，当我们只需要存放元素值时，就选择实现Collection接口的集合，需要保证元素唯一时选择实现Set接口的集合比如TreeSet或HashSet，不需要就选择实现List接口的比如ArrayList或LinkedList，然后再根据实现这些接口的集合的特点来选用。



#### **多线程**



##### **1、何为线程？**
线程是比进程更小的执行单位，一个进程在其执行的过程中可以产生多个线程。与进程不同的是同类的多个线程共享进程的堆和方法区资源，但每个线程有自己的程序计数器、虚拟机栈和本地方法栈，所以系统在产生一个线程，或是在各个线程之间切换工作时，负担要比进程小得多，因此，线程也被成为轻量级进程。
Java程序天生就是多线程程序，我们可以通过JMX来看一下一个普通的Java程序有哪些线程，代码如下：

```Java
public class MultiThread{
	public static void main(String[] args){
		//获取Java线程管理MXBean
		ThreadMXBean threadMXBean = ManagementFactory.getThreadMXBean();
		//不需要获取同步的monitor和synchronizer信息，仅获取线程和线程堆栈信息
		ThreadInfo[] threadInfo = threadMXBean.dumpAllThreads(false,false);
		//遍历线程信息，仅打印线程ID和线程名称信息
		for(ThreadInfo threadInfo:threadInfos){
			System.out.println("["+threadInfo.getThreadId()+"]"+threadInfo.getThreadName());
		}
	}
}
```
上述程序输出如下：
[5] Attach Listener			//添加事件
[4] Signal Dispatcher		//分发处理给JVM信号的线程
[3] Finalizer			//调用对象finalize方法的线程
[2] Reference Handler		//清除reference线程
[1] main				//main线程，程序入口
从上面的输出内容可以看出：一个Java程序的运行是main线程和多个其他线程同时运行。



**2、产生线程死锁须具备的四个条件**

+ 互斥条件：该资源任意一个时刻只由一个线程占用。
+ 请求与保持条件：一个进程因请求资源而阻塞时，对已获得的线程保持不放。
+ 不剥夺条件：线程已获得的资源在未使用完之前不能被其他线程强行剥夺，只有自己使用完毕后才能释放资源。
+ 循环等待条件：若干进程之间形成一种头尾相接的循环等待资源关系。

注：避免线程死锁只要破坏产生死锁的四个条件之一即可。



##### **3、sleep()方法和wait()方法的区别和共同点？**

+ 两者最主要的区别在于：sleep方法没有释放锁，而wait方法释放了锁。
+ wait()通常被用于线程间交互/通信，sleep通常被用于暂停执行。
+ wait()方法被调用后，线程不会自动苏醒，需要别的线程调用同一个对象上的notify()或者notifyAll()方法。sleep()方法执行完成后，线程会自动苏醒。或者可以使用wait(long timeout)超时后线程会自动苏醒。
+ 两者都可以暂停线程的执行。



##### **4、为什么调用start()时会执行run()方法，为什么不能直接调用run()方法？**

new一个Thread，线程就进入了新建状态；调用start()方法，会启动一个线程并使线程进入了就绪状态，当分配到时间片后就可以开始运行了。start()会执行线程的相应准备工作，然后自动执行run()方法的内容，这是真正的多线程工作。而直接执行run()方法，会把run()方法当成一个main线程下的普通方法去执行，并不会在某个线程中执行它，所以这并不是多线程工作。



##### **5、synchronized关键字**

synchronized关键字解决的是多个线程之间访问资源的同步性，synchronized关键字可以保证被它修饰的方法或者代码块在任意时刻只能由一个线程执行。

###### synchronized关键字最主要的三种使用方式：

+ 修饰实例方法：作用于当前对象实例加锁，进入同步代码前要获得当前对象实例的锁。
+ 修饰静态方法：也就是给当前类加锁，会作用于类的所有对象实例，因为静态成员不属于任何一个实例对象，是类成愿（static表明这是该类的一个静态资源，不管new了多少个对象，只有一份）。所以如果一个线程A调用一个实例对象的非静态synchronized方法，而线程B需要调用这个实例对象所属类的静态synchronized方法，是允许的，不会发生互斥现象，因为访问静态synchronized方法占用的锁是当前类的锁，而访问非静态synchronized方法占用的锁是当前实例对象锁。
+ 修饰代码快：指定加锁对象，对给定对象加锁，进入同步代码库前要获得给定对象的锁。