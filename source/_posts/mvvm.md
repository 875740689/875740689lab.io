﻿---
title: 了解MVVM，从Vue实例出发
//toc: true
//thumbnail: img/mvvm.png
date: 2020-03-30 12.47
tags:
 - MVVM
 - Vue
categories:
 - Web前端
---

## MVVM的由来

在过去的几年中，我们已经把很多传统的服务端代码放到了浏览器中，这样就产生了成千上万行的javascript代码，它们连接了各式各样的HTML 和CSS文件，但缺乏正规的组织形式，这也就是为什么越来越多的开发者使用javascript框架。比如：angular、react、vue。<!--more-->浏览器的兼容性问题已经不再是前端的阻碍。前端的项目越来越大，项目的可维护性和扩展性、安全性等成了主要问题。当年为了解决浏览器兼容性问题，出现了很多类库，其中最典型的就是jquery。但是这类库没有实现对业务逻辑的分成，所以维护性和扩展性极差。综上两方面原因，才有了MVVM模式一类框架的出现。比如vue,通过数据的双向绑定，极大了提高了开发效率。

## MVVM的概述

MVVM是Model-View-ViewModel的简写，它是一种前端视图层的分层开发思想，Model指的是传递的数据，View指的是页面的结构，而ViewModel是MVVM模式的核心，它是连接View和Model的桥梁，当数据变化时ViewModel能够监听到，然后使对应的视图做出自动更新，当视图变化使ViewModel也能监听到，从而使数据自动更新，这就实现了数据的双向绑定，这也是MVVM思想的好处。


## 采用MVVM模式的框架Vue

  Vue是一套用于构建用户界面的渐进式JavaScript框架，只关注视图层，方便与第三方库或既有项目整合

代码实例：
```html
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width,initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Document</title>
	<!--导入Vue的包-->
	<script src="./bin/vue-2.4.0.js"></script>
</head>

<body>
	<!--Vue实例所控制的这个元素区域，就是我们的V-->
	<div id="app">
	<p>{{msg}}</p>
</div>
	<script>
		//创建一个Vue实例
		//当我们导入包之后，在浏览器的内存中，就多了一个Vue构造函数
		//new出来的这个vm对象，就是我们MVVM中的VM调度者
		var vm = new Vue({
			el: '#app',			//表示当前new的这个Vue实例，要控制页面上的哪个区域
			//这里的data就是MVVM中的M，专门用来存放数据
			data: {			//data属性中，存放的是el要用到的数据
				msg: 'HelloWorld'	//通过Vue提供的指令，很方便的就能把数据渲染到页面上，程序员不再手动操作DOM元素了
			}
		}）
	</script>
</body>

</html>
```

